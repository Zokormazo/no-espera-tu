﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class HighScoreManager : MonoBehaviour {

    List<HighScore> _highScores;  

    private void Awake() {
        Load();
    }

    private void Save() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/highscore.dat");

        bf.Serialize(file, _highScores);
        file.Close();
    }

    private void Load() {
        if (File.Exists(Application.persistentDataPath + "/highscore.dat")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/highscore.dat", FileMode.Open);
            _highScores = (List<HighScore>)bf.Deserialize(file);
            file.Close();
        } else {
            _highScores = new List<HighScore>();
        }
    }

    public bool isHighScore(int score) {
        return _highScores.Count < 5 || score > _highScores[4].score;
    }

    public void Add(HighScore highScore) {
        if (_highScores.Count < 5) {
            _highScores.Add(highScore);
            _highScores.Sort();
            Save();
        } else if (highScore.score > _highScores[4].score) {
            _highScores[4] = highScore;
            _highScores.Sort();
            Save();
        }
    }

    public List<HighScore> scores {
        get { return _highScores; }
    }
}
