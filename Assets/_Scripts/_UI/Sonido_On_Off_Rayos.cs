﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonido_On_Off_Rayos : MonoBehaviour {

    public AudioSource _miAudioSourceRayos;

    // Update is called once per frame
    void Update ( )
    {

        if ( Input.GetKeyDown ( "1" ) )
        {

             _miAudioSourceRayos.volume = 0.1f;
           
        }
        if ( Input.GetKeyDown ( "2" ) )
        {

            _miAudioSourceRayos.volume = 0.2f;
            
        }
        if ( Input.GetKeyDown ( "3" ) )
        {

            _miAudioSourceRayos.volume = 0.3f;
            
        }
        if ( Input.GetKeyDown ( "4" ) )
        {

            _miAudioSourceRayos.volume = 0.4f;
            
        }
        if ( Input.GetKeyDown ( "5" ) )
        {

            _miAudioSourceRayos.volume = 0.5f;
            
        }
        if ( Input.GetKeyDown ( "6" ) )
        {

            _miAudioSourceRayos.volume = 0.6f;
           
        }
        if ( Input.GetKeyDown ( "7" ) )
        {

            _miAudioSourceRayos.volume = 0.7f;
           
        }
        if ( Input.GetKeyDown ( "8" ) )
        {

            _miAudioSourceRayos.volume = 0.8f;
            
        }
        if ( Input.GetKeyDown ( "9" ) )
        {

            _miAudioSourceRayos.volume = 0.9f;
            
        }
        if ( Input.GetKeyDown ( "0" ) )
        {

            _miAudioSourceRayos.volume = 1f;
            
        }
    }
    }
