﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelEndManager : MonoBehaviour {

    [SerializeField]
    private Text _bloodCountScoreText;
    [SerializeField]
    private Text _killsCountScoreText;
    [SerializeField]
    private Text _timeCountScoreText;
    [SerializeField]
    private Button _nextButton;
    [SerializeField]
    private Text _nextButtonText;

    private int _nextScene;
    private AsyncOperation _gameLoadingOperation;

    // Use this for initialization
    private void Start () {
        Score score = GameManager.LastScore;
        _bloodCountScoreText.text = string.Format("{0:F0}", score.collectedBlood);
        _killsCountScoreText.text = string.Format("{0}", score.kills);
        _timeCountScoreText.text = string.Format("{0}:{1:D2}", (int)(score.totalTime / 60), (int)(score.totalTime % 60));
        _nextScene = GameManager.nextScene;
        if (GameManager.GameMode == GameModes.Level)
            _nextButtonText.text = "Volver";
    }

    private IEnumerator LoadNextSceneAsync() {
        _gameLoadingOperation = SceneManager.LoadSceneAsync(_nextScene);
        yield return _gameLoadingOperation;
    }

    public void GoToNext() {
        StartCoroutine(LoadNextSceneAsync());
    }
}
