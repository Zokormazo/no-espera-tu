﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DeadManager : MonoBehaviour {

    public void loadGameEndScene() {
        SceneManager.LoadScene("GameEnd");
    }
}
