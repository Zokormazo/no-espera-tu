﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonido_On_Off_Music : MonoBehaviour
{

    public AudioSource _miAudioSource;
    private float _volume;

    void Start ( )
    {
        _volume = _miAudioSource.volume;

    }

    // Update is called once per frame
    void Update ( )
    {

        if ( Input.GetKey ( "v" ) && _volume < 1.0f )
        {

            Debug.Log ( "Subo a " + _volume );
            _miAudioSource.volume += 0.01f;
            _volume = _miAudioSource.volume;

        }
        if ( Input.GetKey ( "b" ) && _volume > 0.01f )
        {

            Debug.Log ( "Bajo a " + _volume );
            _miAudioSource.volume -= 0.01f;
            _volume = _miAudioSource.volume;

        }

    }
}
    
