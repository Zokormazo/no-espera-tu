﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour {

    private Text _messageText;
    private Animator _animator;

    private static MessageManager _instance;

    public static void ShowMessage(string message, float time = 3.0f) {
        _instance._messageText.text = message;
        _instance._animator.SetBool("show", true);
        _instance.Invoke("HideMessage", time);
    }

	// Use this for initialization
	private void Awake () {
        _instance = this;
        _messageText = GetComponentInChildren<Text>();
        _messageText.text = string.Empty;
        _animator = _messageText.GetComponent<Animator>();
	}

    private void HideMessage() {
        _animator.SetBool("show", false);
    }
}
