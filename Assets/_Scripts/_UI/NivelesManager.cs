﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NivelesManager : MonoBehaviour {

    [Serializable]
    public class Nivel {
        public string name;
        public Sprite image;
        public string scene;
    }

    [Serializable]
    public class LevelPanel
    {
        public Text _number;
        public Text _title;
        public Image _portrait;
        public Button _button;
    }

    [SerializeField]
    private Nivel[] _levels;

    [SerializeField]
    private Sprite _locked;

    private AudioSource _audioSource;

    [Header("Canvas Elements")]
    [SerializeField]
    private GameObject _previousButton;
    [SerializeField]
    private GameObject _nextButton;
    [SerializeField]
    private LevelPanel[] _panels;

    private int _totalPanels;
    private int _currentPanel;
    private int _unlockedLevels;

    private AsyncOperation _gameLoadingOperation;

	// Use this for initialization
	private void Start () {
        _audioSource = GetComponent<AudioSource>();
        _totalPanels = _levels.Length / 2 + _levels.Length % 2;
        _unlockedLevels = PlayerPrefs.GetInt("Completed Levels");
        UpdateUI();

        Cheat.OnUnlockLevels += Cheat_OnUnlockLevels;
	}

    private void Cheat_OnUnlockLevels() {
        _unlockedLevels = _levels.Length;
        _audioSource.Play();
        UpdateUI();
    }

    private void UpdateUI() {
        _previousButton.SetActive(_currentPanel != 0);
        _nextButton.SetActive(_currentPanel < _totalPanels - 1);
        RenderPanel(0, _currentPanel * 2); // Render left panel
        RenderPanel(1, _currentPanel * 2 + 1); // Render right panel
    }

    private void RenderPanel(int panelIndex, int levelIndex) {
        if (panelIndex >= _panels.Length)
            return;
        if (levelIndex < _levels.Length) {
            _panels[panelIndex]._number.text = string.Format("{0:D2}", levelIndex + 1);
            _panels[panelIndex]._title.text = _levels[levelIndex].name;
            if (levelIndex < _unlockedLevels) {
                _panels[panelIndex]._button.interactable = true;
                _panels[panelIndex]._portrait.sprite = _levels[levelIndex].image;
            }
            else {
                _panels[panelIndex]._button.interactable = false;
                _panels[panelIndex]._portrait.sprite = _locked;
            }
        } else {
            _panels[panelIndex]._number.text = "";
            _panels[panelIndex]._title.text = "";
            _panels[panelIndex]._portrait.sprite = _locked;
        }
    }

    private IEnumerator LoadGameAsync(string level) {
        GameManager.GameMode = GameModes.Level;
        _gameLoadingOperation = SceneManager.LoadSceneAsync(level);
        //_gameLoadingOperation.allowSceneActivation = false;
        yield return _gameLoadingOperation;
    }

    public void LoadLevel(int panelIndex) {
        StartCoroutine(LoadGameAsync(_levels[_currentPanel * 2 + panelIndex].scene));
    }

    public void Next() {
        _currentPanel = Mathf.Min(_totalPanels, _currentPanel + 1);
        UpdateUI();
    }

    public void Previous() {
        _currentPanel = Mathf.Max(0, _currentPanel - 1);
        UpdateUI();
    }
}
