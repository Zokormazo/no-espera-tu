﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Globalization;

public class GameEndManager : MonoBehaviour {

    [SerializeField]
    private float _countingTime = 3.0f;

    [SerializeField]
    private Text _scoreText;

    [SerializeField]
    private GameObject _highScorePanel;

    [SerializeField]
    private InputField _nameInputField;

    [SerializeField]
    private Button _continueButton;

    private int _totalScore;
    private int _showedScore;
    private bool _counterRunning;
    private float _countingSpeed;
    private HighScoreManager _highScoreManager;

	// Use this for initialization
	void Start () {
        _totalScore = GameManager.totalScore;
        _counterRunning = true;
        _countingSpeed = _totalScore / _countingTime;
        _highScoreManager = GetComponent<HighScoreManager>();
        _highScorePanel.SetActive(false);
        _continueButton.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (_counterRunning) {
            _showedScore = Mathf.RoundToInt(Mathf.MoveTowards(_showedScore, _totalScore, _countingSpeed * Time.deltaTime));
            if (_totalScore - _showedScore < 30) {
                _showedScore = _totalScore;
                _counterRunning = false;
                if (_totalScore > 0 && _highScoreManager.isHighScore(_totalScore))
                    _highScorePanel.SetActive(true);
                else
                    _continueButton.gameObject.SetActive(true);
            }
            UpdateScoreText();
        }
	}

    private void UpdateScoreText() {
        _scoreText.text = string.Format("Score: {0} puntos", _showedScore.ToString("N0", CultureInfo.CreateSpecificCulture("es-ES")));
    }

    public void AddHighScore() {
        HighScore highScore = new HighScore(_nameInputField.text, _totalScore);
        _highScorePanel.SetActive(false);
        _continueButton.gameObject.SetActive(true);
        _highScoreManager.Add(highScore);
    }

    public void Continue() {
        SceneManager.LoadScene("Splash");
    }
}
