﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OcultarOpciones : MonoBehaviour {

    public bool startHidden;
    public GameObject MenuCanvas;
    private bool _seve;

    void Start ( )
    {

        MenuCanvas.SetActive ( !startHidden );
        _seve = true;

    }

    // Update is called once per frame
    void Update ( )
    {
        if ( Input.GetKeyDown ( "m" ) )
        {
            if ( _seve == true )
            {
                MenuCanvas.SetActive ( false );
                _seve = false;

            }
            else if ( _seve == false )
            {
                MenuCanvas.SetActive ( true );
                _seve = true;
            }

        }


           
    }
    
    }
