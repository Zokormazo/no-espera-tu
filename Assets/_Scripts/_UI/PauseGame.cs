﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {

    private bool _quieto;


    void Start ( )
    {

        _quieto = false;

    }

    // Update is called once per frame
    void Update ( )
    {

        if ( Input.GetKeyDown ( "p" ) && _quieto == false )
        {
            _quieto = true;
            Time.timeScale = 0;

        }
        else if ( Input.GetKeyDown ( "p" ) && _quieto == true )
        {
            _quieto = false;
            Time.timeScale = 1;

        }

    }
    }
