﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * Botones:
 * 0: Introduccion
 * 1: Documento de Juego
 * 2: Personajes
 * 3: Partida nueva
 * 4: Controles
 * 5: Records
 * 6: Niveles
 * 7: Creditos
 * 8: Continuar partida
 */

[Serializable]
public class ButtonInfo {
    public string _scene;
    public Image _progressImage;
    public Text _progressText;
}

 public class SplashManager : MonoBehaviour {

    [SerializeField]
    private ButtonInfo[] _buttons;

    [SerializeField]
    private GameObject _newButton;
    [SerializeField]
    private GameObject _continueButton;

    private bool _buttonsShown;

    private int _loadingScene;
    private AsyncOperation _gameLoadingOperation;

    private IEnumerator LoadGameAsync(string scene) {
        _gameLoadingOperation = SceneManager.LoadSceneAsync(scene);
        yield return _gameLoadingOperation;
    }

    private IEnumerator LoadGameAsync(int scene) {
        _gameLoadingOperation = SceneManager.LoadSceneAsync(scene);
        yield return _gameLoadingOperation;
    }

    private void Update() {
        if (_gameLoadingOperation != null) {
            if (_buttons[_loadingScene]._progressImage != null)
                _buttons[_loadingScene]._progressImage.fillAmount = _gameLoadingOperation.progress;
            if (_buttons[_loadingScene]._progressText != null) {
                _buttons[_loadingScene]._progressText.text = string.Format("{0:P0}", _gameLoadingOperation.progress);
            }
        }
    }

    public void Quit() {
        Application.Quit();
    }

    public void ExecButton(int buttonId) {
        _loadingScene = buttonId;
        if (buttonId == 8) {
            int levelToLoad = PlayerPrefs.GetInt("Current Level") * 2 + 8;
            GameManager gameManager = FindObjectOfType<GameManager>();
            gameManager.LoadScore();
            StartCoroutine(LoadGameAsync(levelToLoad));
        } else
            StartCoroutine(LoadGameAsync(_buttons[buttonId]._scene));
    }

    public void ToggleButtons() {
        _buttonsShown = !_buttonsShown;
        _newButton.SetActive(_buttonsShown);
        if (PlayerPrefs.GetInt("Current Level") > 0 && PlayerPrefs.GetInt("Current Level") < 10) {
            _continueButton.SetActive(_buttonsShown);
        }
    }
}
