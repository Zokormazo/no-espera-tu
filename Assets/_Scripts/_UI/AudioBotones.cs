﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AudioBotones : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header ( "Configuración de sonido" )]
    //Declaramos la variable del sonido de la pantalla de inicio
    public AudioClip _AudioBoton;

    public AudioSource SoundManager;


    public void OnPointerEnter ( PointerEventData ped )
    {
        SoundManager.Play ();
    }

    public void OnPointerExit ( PointerEventData ped )
    {
        SoundManager.Stop ( );
    }
}

