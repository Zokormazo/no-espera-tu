﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheat : MonoBehaviour {

    public delegate void UnlockLevels();
    public static event UnlockLevels OnUnlockLevels;

    private int _touches;
    private bool _fired;

	private void OnDestroy () {
        OnUnlockLevels = null;
	}

    private void OnMouseDown() {
        if (!_fired) {
            _touches++;
            if (_touches > 4 && OnUnlockLevels != null) {
                OnUnlockLevels();
                _fired = true;
            }
        }
    }
}
