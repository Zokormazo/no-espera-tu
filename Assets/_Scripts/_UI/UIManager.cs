﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager: MonoBehaviour {

    [Header("Dracula")]
    [Tooltip("Text component for Dracula's health")]
    [SerializeField]
    private Text _draculaHealthText;
    [Tooltip("Image component for Dracula's health")]
    [SerializeField]
    private Image _draculaHealthImage;

    [Header("Citizen")]
    [Tooltip("Text component for citizen's name")]
    [SerializeField]
    private Text _citizenNameText;
    [Tooltip("Text component for citizen's health")]
    [SerializeField]
    private Text _citizenHealthText;
    [Tooltip("Portrait photo holder for citizen's photo")]
    [SerializeField]
    private RawImage _citizenPhoto;

    [Header("Loading bar")]
    [Tooltip("Loading bar main GameObject")]
    [SerializeField]
    private GameObject _loadingBar;
    [Tooltip("Text component for the loading bar")]
    [SerializeField]
    private Text _loadingText;
    [Tooltip("Image component for the loading bar")]
    [SerializeField]
    private Image _loadingImage;

    private CitizenController _selectedCitizen;

	// Use this for initialization
	private void Awake () {
        CitizenController.OnShowStats += CitizenController_OnShowStats;
        DraculaController.OnBloodChange += DraculaController_OnBloodChange;
	}

    private void Start() {
        GameManager.loadingBar = _loadingBar;
        GameManager.loadingImage = _loadingImage;
        GameManager.loadingText = _loadingText;
    }

    private void DraculaController_OnBloodChange(float delta, float absolute, float max) {
        DraculaRenderHealthBar(absolute, max);
    }

    private void CitizenController_OnShowStats(CitizenController citizen) {
        if (_selectedCitizen) {
            _selectedCitizen.selected = false;
            _selectedCitizen.OnUpdateHealthBar -= Citizen_OnUpdateHealthBar;
        }
        _selectedCitizen = citizen;
        citizen.selected = true;
        citizen.OnUpdateHealthBar += Citizen_OnUpdateHealthBar;
        _citizenNameText.text = citizen.citizenName;
        _citizenPhoto.texture = citizen.citizenPhoto;
        _citizenPhoto.enabled = true;
        CitizenRenderHealthBar();
    }

    private void Citizen_OnUpdateHealthBar() {
        CitizenRenderHealthBar();
    }

    private void CitizenRenderHealthBar() {
        if (_citizenHealthText)
            _citizenHealthText.text = string.Format("{0}/{1}", _selectedCitizen.currentBlood, _selectedCitizen.maxBlood);
    }

    // Render Health Bar on UI
    private void DraculaRenderHealthBar(float current, float max) {
        if (_draculaHealthImage != null)
            _draculaHealthImage.fillAmount = current / max;
        if (_draculaHealthText != null)
            _draculaHealthText.text = string.Format("{0}/{1}", (int)current, (int)max);
    }
}
