﻿using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class RecordsManager : MonoBehaviour {

    [SerializeField]
    private Text[] _playerNames;
    [SerializeField]
    private Text[] _scores;

    private HighScoreManager _highScoreManager;
    private List<HighScore> _highScores;

	// Use this for initialization
	void Start () {
        _highScoreManager = GetComponent<HighScoreManager>();
        _highScores = _highScoreManager.scores;
        for (int i = 0; i < _highScores.Count; i++) {
            _playerNames[i].text = _highScores[i].playerName;
            _scores[i].text = string.Format("{0} puntos", _highScores[i].score.ToString("N0", CultureInfo.CreateSpecificCulture("es-ES")));
        }
	}
}
