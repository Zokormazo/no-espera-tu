﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door_2 : MonoBehaviour {

    private Animator _animator;
    private BoxCollider2D _collider;

    private void Start() {
        _animator = GetComponent<Animator>();
        _collider = GetComponent<BoxCollider2D>();
    }

    public void open() {
        _animator.SetBool("Puerta", true);
        if (_collider)
            _collider.enabled = false;
    }

    public void close() {
        _animator.SetBool("Puerta", false);
        if (_collider)
            _collider.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Dracula")
            open();
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == "Dracula")
            close();
    }
}
