﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class HideableObject : MonoBehaviour {

    // Components
    private Collider2D objectCollider2D;
    private SpriteRenderer objectSpriteRenderer;

	// Use this for initialization
	private void Start () {
        objectCollider2D = GetComponent<Collider2D>();
        objectSpriteRenderer = GetComponent<SpriteRenderer>();

        objectCollider2D.isTrigger = true;
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Dracula")
            objectSpriteRenderer.enabled = false;
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == "Dracula")
            objectSpriteRenderer.enabled = true;
    }
}
