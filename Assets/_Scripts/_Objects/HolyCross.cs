﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolyCross : MonoBehaviour {

    [Header("Slow down effect")]
    [Tooltip("Power")]
    [SerializeField]
    private float power;

    private void OnTriggerEnter2D(Collider2D colider) {
        if (colider.tag == "Dracula")
            colider.GetComponent<DraculaController>().Blind(power);
    }

    private void OnTriggerExit2D(Collider2D colider) {
        if (colider.tag == "Dracula")
            colider.GetComponent<DraculaController>().UnBlind();
    }
}
