﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideOther : MonoBehaviour {

    [SerializeField]
    private GameObject _gameObjectToHide;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Dracula")
            _gameObjectToHide.SetActive(false);
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == "Dracula")
            _gameObjectToHide.SetActive(true);
    }
}
