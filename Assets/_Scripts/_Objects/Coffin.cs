﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CircleCollider2D))]
public class Coffin : MonoBehaviour {

    private Animator _animator;
    [SerializeField]
    private bool _openInStart;

    private void Start() {
        _animator = GetComponent<Animator>();
        if (_openInStart)
            open();
    }

    public void open() {
        _animator.SetBool("Ataud", true);
    }

    public void close() {
        _animator.SetBool("Ataud", false);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Dracula")
            open();
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == "Dracula")
            close();
    }
}
