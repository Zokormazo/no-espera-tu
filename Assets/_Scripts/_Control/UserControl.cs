﻿/*
 * UserControl.cs
 * Controls movement of a character with user input
 */
using UnityEngine;

public class UserControl : Control {

    // Private variables
    private DraculaController draculaController;

    // Use this for initialization
    private void Start () {
        if (gameObject.tag == "Dracula") // We are controlling Dracula
            draculaController = GetComponent<DraculaController>();
	}

    // Physics related Update
    private void FixedUpdate() {
        // Get Movement Input
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (horizontal == 0 && vertical == 0)
            Stop();
        else
            Move(new Vector2(horizontal, vertical)); // Move with CharacterMovementController
    }

    // Main Loop
    private void Update() {
        if (canExecuteActions) {
            if (draculaController) {
                if (Input.GetMouseButtonDown(0)) {
                    draculaController.Bite(true);
                }

                if (Input.GetMouseButtonUp(0)) {
                    draculaController.Bite(false);
                }

                if (Input.GetMouseButtonDown(1)) {
                    draculaController.Fly(true);
                }
            }
        }

        if (draculaController) {
            if (Input.GetMouseButtonUp(1)) {
                draculaController.Fly(false);
            }
        }

        if (draculaController.state != CharacterState.Flying) {
            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
                characterController.running = true;
            if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift))
                characterController.running = false;

            if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl)) {
                if (draculaController.state != CharacterState.Carrying) {
                    draculaController.PickUp();
                }
                else {
                    draculaController.Drop();
                }
            }
        }
    }
}
