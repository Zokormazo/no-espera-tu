﻿/* 
 * Control.cs
 * Character controlling parent class
 */
using UnityEngine;

[RequireComponent(typeof(MyCharacterController))]
abstract public class Control : MonoBehaviour {

    // Components
    protected MyCharacterController characterController;

    // Available actions
    protected bool canMove;
    protected bool canExecuteActions;

    // Early initialization
    protected void Awake() {
        characterController = GetComponent<MyCharacterController>(); // Get CharacterController for later usage
        CharacterController_OnStateChange(characterController.state); // Initialize state with CharacterController's state
        characterController.OnStateChange += CharacterController_OnStateChange; // Subscribe to CharacterController's OnStateChange event
    }

    // OnStateChange event callback
    protected virtual void CharacterController_OnStateChange(CharacterState state) {
        switch(state) {
            case CharacterState.Normal: // Normal: can move & exec actions
            case CharacterState.Escaping:
                canMove = true;
                canExecuteActions = true;
                break;
            case CharacterState.Immobilized: // Immobilized: can't move but can exec actions
                canMove = false;
                canExecuteActions = true;
                break;
            case CharacterState.Flying:
                canExecuteActions = false;
                canMove = true;
                break;
            case CharacterState.Stunned: // Dead or Stunned: can't move nor exec actions
            case CharacterState.Dead:
                canMove = false;
                canExecuteActions = false;
                break;
        }
    }

    // Move the controlling character on given direction
    public void Move(Vector2 _direction) {
        characterController.Move(_direction);
    }

    // Stop movement of the controlling character
    public void Stop() {
        characterController.Stop();
    }
}
