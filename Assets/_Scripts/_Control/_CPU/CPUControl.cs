﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class CPUControl : Control
{

    // Variables exported to Unity Editor's Inspector
    [Header("Vision")]
    [Tooltip("Range")]
    [SerializeField]
    private float visionRange = 2f;
    [Tooltip("Aperture of vision, in degrees")]
    [SerializeField]
    private float visionAperture = 45f;
    [Tooltip("Vision rotation speed, in degrees per second")]
    [SerializeField]
    private float visionRotationSpeed = 45f;
    [Tooltip("Offset of vision position radius")]
    [SerializeField]
    private float visionPositionRadiusOffset = 0f;
    [Tooltip("Wether vision system is enabled")]
    [SerializeField]
    private bool visionEnabled;

    [Header("Gizmos")]
    [Tooltip("When to draw gizmos on Editor")]
    [SerializeField]
    private EnumDrawGizmos drawGizmos;
    [Tooltip("Drawing vision ray on editor")]
    [SerializeField]
    bool drawVisionRayGizmo;
    [Tooltip("Color of vision ray")]
    [SerializeField]
    Color colorVisionRayGizmo = Color.white;

    // Components
    protected Rigidbody2D characterRigidbody2D;
    protected BoxCollider2D characterBoxCollider2D;
    protected MyCharacterController _characterController;

    // Vision related variables
    protected bool canSee;
    private float visionCurrentAngle;
    private bool visionIsDecrementingAngle;
    private float visionPositionRadius;

    // Use this for initialization
    protected void Start() {
        characterRigidbody2D = GetComponent<Rigidbody2D>();
        characterBoxCollider2D = GetComponent<BoxCollider2D>();
        _characterController = GetComponent<MyCharacterController>();
        CalculateVisionPositionRadius();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (visionEnabled && canSee && collision.tag == "Dracula") {
            OnSeenDracula(collision);
        }
    }

    // Calculate eye position for vision system. We need eyes outside our collider to avoid hitting it as first element.
    private void CalculateVisionPositionRadius() {
        Vector2 vector = characterBoxCollider2D.size / 2;
        visionPositionRadius = (vector - characterBoxCollider2D.size).magnitude + visionPositionRadiusOffset;
    }


    // Update vision's current angle based on looking direction, current looking angle and rotation speed
    private void UpdateAngle() {
        float angleDelta = visionRotationSpeed * Time.deltaTime;
        if (visionIsDecrementingAngle)
            angleDelta = -angleDelta;
        visionCurrentAngle += angleDelta;
        if (!visionIsDecrementingAngle && visionCurrentAngle > visionAperture / 2) {
            visionCurrentAngle = visionAperture / 2;
            visionIsDecrementingAngle = true;
        }
        else if (visionIsDecrementingAngle && visionCurrentAngle < -visionAperture / 2) {
            visionCurrentAngle = -visionAperture / 2;
            visionIsDecrementingAngle = false;
        }
    }

    // Rotate a vector given angle degrees,
    private Vector2 RotateVector(Vector2 direction, float angle) {
        Vector2 result = new Vector2();
        double rad = angle * (float)(Math.PI / 180);
        result.x = (float)(direction.x * Math.Cos(rad) - direction.y * Math.Sin(rad));
        result.y = (float)(direction.x * Math.Sin(rad) + direction.y * Math.Cos(rad));
        return result.normalized;
    }

    // Exec vision system
    public void Vision(Vector2 direction) {
        if (visionEnabled && canSee) {
            UpdateAngle();
            direction = RotateVector(direction, visionCurrentAngle).normalized;
            Vector2 origin = (Vector2)transform.position + direction * visionPositionRadius;
            Debug.DrawRay(origin, direction * visionRange, colorVisionRayGizmo);
            int layerMask = ~LayerMask.GetMask("TransparentObjects");
            RaycastHit2D raycastResult = Physics2D.Raycast(origin, direction, visionRange, layerMask);
            if (raycastResult) {
                if (raycastResult.collider.tag == "Dracula")
                    OnSeenDracula(raycastResult.collider);
                else if (raycastResult.collider.tag == "Citizen") {
                    CitizenController citizen = raycastResult.collider.GetComponent<CitizenController>();
                    if (!citizen.alive) {
                        OnSeenCorpse(raycastResult, citizen.gameObject.GetInstanceID());
                    } else if (citizen.injured && citizen.state != CharacterState.Escaping) {
                        OnSeenInjuredCitizen(raycastResult);
                    }
                }
            }
        }
    }

    // OnStateChange event callback
    protected override void CharacterController_OnStateChange(CharacterState state) {
        base.CharacterController_OnStateChange(state);
        switch(state) {
            case CharacterState.Normal:
            case CharacterState.Immobilized:
                canSee = true;
                break;
            case CharacterState.Stunned:
            case CharacterState.Dead:
                canSee = false;
                break; 
        }
    }

    public virtual float meleeDistance {
        get { return _characterController.meleeDistance; }
    }

    // Return to patrol state
    public virtual void GoToPatrol() { }

    // OnSeenDracula event callback
    protected virtual void OnSeenDracula(Collider2D collider) { }

    // OnSeenCorpse event callback
    protected virtual void OnSeenCorpse(RaycastHit2D hitInfo, int instanceID) { }

    // OnSeenInjuredCitizen event callback
    protected virtual void OnSeenInjuredCitizen(RaycastHit2D hitInfo) { }

    public float VisionPositionRadius {
        get { return visionPositionRadius; }
    }
}