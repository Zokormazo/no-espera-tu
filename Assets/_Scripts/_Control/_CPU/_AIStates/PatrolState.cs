﻿/* 
 * PatrolState.cs
 * AI State for patrolling between waypoints.
 * Patrolling path IS NOT calculated with pathfinding
 */
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IState {

    // Variables
    private CPUControl control; // Reference to the current instance of CPUControl
    private Vector2[] waypoints; // Patrolling path waypoints
    private int currentWaypoint; // Current waypoint on path

    // State's main loop method
    public void UpdateState() {
        Vector2 moveDirection = waypoints[currentWaypoint] - (Vector2)control.transform.position; // Calculate movement direction
        control.Vision(moveDirection); // Look at moving direction
        if (moveDirection.magnitude < GameConf.WaypointReachedConsideredDistance) // We reached a waypoint
            currentWaypoint = (currentWaypoint + 1) % waypoints.Length; // Set next waypoint as current waypoint
        else // We haven't reached next waypoint yet
            control.Move(moveDirection); // Move on calculated direction
    }

    // Draw gizmos on Scene Editor
    public void OnDrawGizmos() {}

    // Constructor Class
    public PatrolState(CPUControl _control, PathDefinition _patrolPath) {
        control = _control;
        waypoints = _patrolPath.waypoints;
        currentWaypoint = 0;
    }

    // Return current waypoint's World position
    public Vector2 GetCurrentWaypoint {
        get { return waypoints[currentWaypoint]; }
    }
}
