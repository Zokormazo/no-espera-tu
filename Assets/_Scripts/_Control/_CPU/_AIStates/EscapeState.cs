﻿/* 
 * EscapeState.cs
 * AI State to scape home. When reached, go to hidden state
 */
using UnityEngine;

public class EscapeState : IState
{
    // Variables
    private CitizenCPUControl control; // Reference to the current instance of CPUControl
    private Animator citizenAnimator;
    private Vector2[] waypoints; // Target path waypoints
    private int currentWaypoint; // Current waypoint on path

    public void UpdateState() {
        if (currentWaypoint < waypoints.Length) { // We are not on the last waypoint
            Vector2 moveDirection = waypoints[currentWaypoint] - (Vector2)control.transform.position; // Calculate movement direction
            control.Vision(moveDirection); // Look at moving direction
            if (moveDirection.magnitude < GameConf.WaypointReachedConsideredDistance) // We reached a waypoint
                currentWaypoint++; // Set next waypoint as current waypoint
            else // We haven't reached next waypoint yet
                control.Move(moveDirection.normalized); // Move on calculated direction. I pass normalized direction to avoid going slower near waypoints
        } else {
            control.Stop();
            citizenAnimator.SetInteger("Accion", (int)CitizenAnimationActions.Parado);
            control.running = false;
            HiddenState newState = new HiddenState(control, control.TimeToHide);
            control.CurrentState = newState;
        }
            
    }

    // Draw gizmos on Scene Editor
    public void OnDrawGizmos() { }

    // Constructor Class
    public EscapeState(CPUControl _control, Vector2[] _path) {
        control = (CitizenCPUControl)_control;
        control.running = true;
        waypoints = _path;
        citizenAnimator = control.GetComponent<Animator>();
        citizenAnimator.SetInteger("Accion", (int)CitizenAnimationActions.Huyendo);
    }
}