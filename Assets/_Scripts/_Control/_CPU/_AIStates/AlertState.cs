﻿/* 
 * AlertState.cs
 * AI State for respond to a positional alert.
 * Character goes to alert point and looks around for a given time after reaching there.
 */
using System;
using UnityEngine;

public class AlertState : IState {

    // Variables
    private CopCPUControl control; // Reference to the current instance of CopCPUControl
    private Vector2[] waypoints; // Target path waypoints
    private int currentWaypoint; // Current waypoint on path
    private float alertPointDuration; // Time to wait on alert point before returning
    private float reachingTime; // Time when we reach alert point
    private float alertPointCurrentTime; // Time elapsed on alert point

    // State's main loop method
    public void UpdateState() {
        if (currentWaypoint < waypoints.Length) { // We are not on the last waypoint
            Vector2 moveDirection = waypoints[currentWaypoint] - (Vector2)control.transform.position; // Calculate movement direction
            control.Vision(moveDirection); // Look at moving direction
            if (moveDirection.magnitude < GameConf.WaypointReachedConsideredDistance) // We reached a waypoint
                currentWaypoint++; // Set next waypoint as current waypoint
            else // We haven't reached next waypoint yet
                control.Move(moveDirection.normalized); // Move on calculated direction. I pass normalized direction to avoid going slower near waypoints
        } else { // Reached alert point
            control.Stop(); // Reached target. Stop moving
            if (reachingTime == 0) { // We just reached alert point
                reachingTime = Time.time; // Store reaching time;
                control.MarkCorpseAsSeen(); // Mark corpse as seen
            } else if (alertPointCurrentTime > alertPointDuration) { // We already stayed for duration
                ToPatrolState(); // Return to patrol state
            }
            alertPointCurrentTime += Time.deltaTime; // Increment elapsed time
            // Generate looking direction to look on each direction for the same time
            Vector2 direction; 
            if (alertPointCurrentTime < alertPointDuration / 4)
                direction = Vector2.up;
            else if (alertPointCurrentTime < alertPointDuration / 2)
                direction = Vector2.right;
            else if (alertPointCurrentTime < alertPointDuration * 3 / 4)
                direction = Vector2.down;
            else
                direction = Vector2.left;
            control.Vision(direction); // Look at direction 
        }
    }

    // Draw gizmos on Scene Editor
    public void OnDrawGizmos() { }

    // Go to patrol state
    public void ToPatrolState() {
        PathRequestManager.RequestPath((Vector2)control.transform.position, control.PatrolState.GetCurrentWaypoint, ToPatrolPathCallback);
    }

    // It's executed when PathRequestManager return a path execution
    private void ToPatrolPathCallback(Vector2[] path, bool success) {
        if (!success) {
            path = new Vector2[1];
            path[0] = control.PatrolState.GetCurrentWaypoint;
            control.Invoke("GoToPatrol", GameConf.RetryPathfindingTime);
        }

        ReturnToPatrolState newState = new ReturnToPatrolState(control, path);
        control.CurrentState = newState;
    }

    // Constructor Class
    public AlertState(CPUControl _control, Vector2[] _path, float _alertPointDuration, int _currentWaypoint = 0) {
        control = (CopCPUControl)_control;
        waypoints = _path;
        alertPointDuration = _alertPointDuration;
        currentWaypoint = _currentWaypoint;
    }
}
