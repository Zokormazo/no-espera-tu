﻿/* 
 * IState.cs
 * Defines character's AI state skeleton interface
 */
public interface IState {
    void UpdateState(); // State's main loop method
    void OnDrawGizmos(); // Draw gizmos on Scene Editor
}
