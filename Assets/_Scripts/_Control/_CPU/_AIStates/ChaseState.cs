﻿/* 
 * ChaseState.cs
 * AI State for chasing a given target.
 * If we stop seeing the target, change to Alert State on the target lastest calculated chasing path.
 */
using UnityEngine;

public class ChaseState : IState
{

    // Variables
    private CopCPUControl control; // Reference to the current instance of CPUControl
    private Transform target; // Target's transform
    private Vector2[] waypoints; // Chasing path waypoints
    private int currentWaypoint; // Current waypoint on path
    private float lastRecalculatePathTime; // Last time we recalculated a path
    private LayerMask chaseLayerMask; // Chasing-vision raycast layer mask
    private float alertPointDuration; // Time to inspect alert point
    private float chaseVisionRange; // Distance of vision of target

    // State's main loop method
    public void UpdateState() {
        if (currentWaypoint < waypoints.Length) { // We are not on the last waypoint
            Vector2 moveDirection = waypoints[currentWaypoint] - (Vector2)control.transform.position; // Calculate movement direction
            if (moveDirection.magnitude < GameConf.WaypointReachedConsideredDistance)  // We reached a waypoint 
                currentWaypoint++; // Set next waypoint as current waypoint
            else // We haven't reached next waypoint yet
                control.Move(moveDirection.normalized); // Move on calculated direction. I pass normalized direction to avoid going slower near waypoints
        } else { // Last watpoint reached
            Vector2 moveDirection = (Vector2)target.position - (Vector2)control.transform.position;
            if (moveDirection.magnitude < control.meleeDistance)
                control.Stop();
            else
                control.Move(target.position - control.transform.position);
        }

        // Since target is moving, recalculate chasing path every timeDeltaRecalculatePath 0.5 if we continue seeing it
        // If we lost direct watch line with target, go to alert state with current path
        Vector2 direction = (target.position - control.transform.position).normalized;
        Vector2 origin = (Vector2)control.transform.position + direction * control.VisionPositionRadius;
        RaycastHit2D hitInfo = Physics2D.Raycast(origin, direction, chaseVisionRange, chaseLayerMask); // Exec seeing check raycast
        Debug.DrawRay(origin, direction * chaseVisionRange, Color.red);
        if (hitInfo && hitInfo.collider.gameObject.GetInstanceID() == target.gameObject.GetInstanceID()) { // We continue seeing target
            if (lastRecalculatePathTime == 0 || Time.time - lastRecalculatePathTime > GameConf.RetryPathfindingTime) // It's time to recalculate path
                PathRequestManager.RequestPath(control.transform.position, target.position, RecalculatePathCallback); // Recalculate Path
            control.Fire(target); // Try to fire against dracula
        } else { // We
            control.running = false;
            ToAlertState(); // Go to alert state
        }
    }

    // It's executed when PathRequestManager return a path execution
    private void RecalculatePathCallback(Vector2[] path, bool success) {
        if (success) { // We have a valid path
            waypoints = path; // Update current path with new path
            currentWaypoint = 0; // Initialize waypoint count
            lastRecalculatePathTime = Time.time; // Store recalculation time
        }
    }

    // Draw gizmos on Scene Editor
    public void OnDrawGizmos() { }

    // Go to alert state
    public void ToAlertState() {
        control.CanSee = true;
        AlertState newState = new AlertState(control, waypoints, alertPointDuration, currentWaypoint); // Create the Alert state
        control.CurrentState = newState; // Set the just created state as current state
    }

    // Constructor Class
    public ChaseState(CopCPUControl _control, Transform _target, Vector2[] _path, float _alertPointDuration, float _chaseVisionRange) {
        control = _control;
        control.running = true;
        target = _target;
        waypoints = _path;
        alertPointDuration = _alertPointDuration;
        chaseLayerMask = ~LayerMask.GetMask("TransparentObjects");
        chaseVisionRange = _chaseVisionRange;
    }
}
