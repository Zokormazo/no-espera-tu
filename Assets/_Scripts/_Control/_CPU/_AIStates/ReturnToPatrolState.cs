﻿/* 
 * ReturnToPatrolState.cs
 * AI State for returning to patrol.
 * It continues given returning path and changes to Patrol State when reached at target point
 */
using UnityEngine;

public class ReturnToPatrolState : IState {

    // Variables
    private CPUControl control; // Reference to the current instance of CPUControl
    private Vector2[] waypoints; // Patrolling path waypoints
    private int currentWaypoint; // Current waypoint on path

    // State's main loop method
    public void UpdateState() {
        if (currentWaypoint < waypoints.Length) { // We are not on the last waypoint
            Vector2 moveDirection = waypoints[currentWaypoint] - (Vector2)control.transform.position; // Calculate movement direction
            control.Vision(moveDirection); // Look at moving direction
            if (moveDirection.magnitude < GameConf.WaypointReachedConsideredDistance) // We reached a waypoint
                currentWaypoint++; // Set next waypoint as current waypoint
            else // We haven't reached next waypoint yet
                control.Move(moveDirection.normalized); // Move on calculated direction. I pass normalized direction to avoid going slower near waypoints
        }
        else // Last waypoint reached
            control.GoToPatrol(); // Go to patrol
    }

    // Draw gizmos on Scene Editor
    public void OnDrawGizmos() { }

    // Constructor Class
    public ReturnToPatrolState(CPUControl _control, Vector2[] _waypoints) {
        control = _control;
        waypoints = _waypoints;
    }
}
