﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoingToHealState : IState {

    // Variables
    private DoctorCPUControl control; // Reference to the current instance of CPUControl
    private Vector2[] waypoints; // Patrolling path waypoints
    private int currentWaypoint; // Current waypoint on path
    private CitizenCPUControl target;

    // State's main loop method
    public void UpdateState() {
        if (currentWaypoint < waypoints.Length) { // We are not on the last waypoint
            Vector2 moveDirection = waypoints[currentWaypoint] - (Vector2)control.transform.position; // Calculate movement direction
            control.Vision(moveDirection); // Look at moving direction
            if (moveDirection.magnitude < GameConf.WaypointReachedConsideredDistance) // We reached a waypoint
                currentWaypoint++; // Set next waypoint as current waypoint
            else // We haven't reached next waypoint yet
                control.Move(moveDirection.normalized); // Move on calculated direction. I pass normalized direction to avoid going slower near waypoints
        }
        else { // Last waypoint reached
            Vector2 moveDirection = (Vector2)target.transform.position - (Vector2)control.transform.position;
            if (moveDirection.magnitude < GameConf.WaypointReachedConsideredDistance)
                control.Stop();
            //FIXIT: Go to heal state
            else
                control.Move(target.transform.position - control.transform.position);
        }
    }

    // Draw gizmos on Scene Editor
    public void OnDrawGizmos() { }

    // Constructor Class
    public GoingToHealState(DoctorCPUControl _control, Vector2[] _waypoints, CitizenCPUControl _target) {
        control = _control;
        waypoints = _waypoints;
        target = _target;
        target.DoctorComing(control);
    }
}
