﻿/* 
 * HiddenState.cs
 * AI State to hide for a while.
 */
using UnityEngine;

public class HiddenState : IState {

    private CitizenCPUControl control; // Reference to the current instance of CopCPUControl
    private float timeToHide; // Time to hide
    private float currentHiddenTime; // Current hidden time

    public void UpdateState() {
        if (currentHiddenTime > timeToHide) {
            control.Show(true);
            ToPatrolState();
        }
        currentHiddenTime += Time.deltaTime;
    }

    // Draw gizmos on Scene Editor
    public void OnDrawGizmos() { }

    // Go to patrol state
    public void ToPatrolState() {
        PathRequestManager.RequestPath((Vector2)control.transform.position, control.PatrolState.GetCurrentWaypoint, ToPatrolPathCallback);
    }

    // It's executed when PathRequestManager return a path execution
    private void ToPatrolPathCallback(Vector2[] path, bool success) {
        if (!success) {
            path = new Vector2[1];
            path[0] = control.transform.position;
            control.Invoke("GoToPatrol", GameConf.RetryPathfindingTime);
        }
        if (success) { // We have a valid path
            control.Show(true);
            ReturnToPatrolState newState = new ReturnToPatrolState(control, path);
            control.CurrentState = newState;
        }
    }

    // Constructor Class
    public HiddenState(CPUControl _control, float _timeToHide) {
        control = (CitizenCPUControl)_control;
        timeToHide = _timeToHide;
        control.Show(false);
    }
}
