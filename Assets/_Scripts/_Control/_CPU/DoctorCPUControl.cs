﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoctorCPUControl : CopCPUControl {

    private CitizenCPUControl healTarget;

    private new void Start() {
        base.Start();
    }

    private void Heal(Transform target) {
        healTarget = target.GetComponent<CitizenCPUControl>();
        PathRequestManager.RequestPath(transform.position, target.position, HealPathCallback);
    }

    private void RetryHeal() {
        PathRequestManager.RequestPath(transform.position, healTarget.transform.position, HealPathCallback);
    }

    private void HealPathCallback(Vector2[] path, bool success) {
        if (!success) {
            path = new Vector2[1];
            path[0] = healTarget.transform.position;
            Invoke("RetryHeal", GameConf.RetryPathfindingTime); 
        }
        GoingToHealState newState = new GoingToHealState(this, path, healTarget);
        currentState = newState;
    }

    public override void OnAlertCorpse(Vector2 position, int instanceID) { }

    protected override void OnSeenCorpse(RaycastHit2D hitInfo, int instanceID) { }

    protected override void OnSeenInjuredCitizen(RaycastHit2D hitInfo) {
        //Heal(hitInfo.transform);
    }
}
