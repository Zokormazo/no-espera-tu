﻿using System.Collections.Generic;
using UnityEngine;

public class CopCPUControl : CPUControl {

    // Variables exported to Unity Editor's Inspector
    [Header("Patrol State")]
    [Tooltip("Path")]
    [SerializeField]
    private PathDefinition patrolPath;

    [Header("Alert State")]
    [Tooltip("Time to investigate alert point")]
    [SerializeField]
    private float alertPointDuration;

    [Header("Chase State")]
    [Tooltip("Chase vision range")]
    [SerializeField]
    private float chaseVisionRange;
    
    // State Machine & States related variables
    protected IState currentState;
    public PatrolState patrolState;
    private int alertCorpseID;
    private HashSet<int> seenCorpses;
    private Transform chaseTarget;

    Vector2 position;

    private new void Awake() {
        base.Awake();
        patrolState = new PatrolState(this, patrolPath);
        seenCorpses = new HashSet<int>();
    }

    protected new void Start() {
        base.Start();
        currentState = patrolState;
    }

    private void Update() {
        currentState.UpdateState();
    }

    private void Alert(Vector2 position) {
        PathRequestManager.RequestPath((Vector2)transform.position, position, AlertPathCallback);
        this.position = position;
    }

    private void RetryAlert() {
        PathRequestManager.RequestPath((Vector2)transform.position, position, AlertPathCallback);
    }

    private void AlertPathCallback(Vector2[] path, bool success) {
        if(!success) {
            path = new Vector2[1];
            path[0] = position;
            Invoke("RetryAlert", GameConf.RetryPathfindingTime);
        } 

        AlertState newState = new AlertState(this, path, alertPointDuration);
        currentState = newState;
    }

    private void Chase(Transform target) {
        chaseTarget = target;
        PathRequestManager.RequestPath((Vector2)transform.position, (Vector2)target.transform.position, ChasePathCallback);
    }

    private void RetryChase() {
        PathRequestManager.RequestPath((Vector2)transform.position, (Vector2)chaseTarget.transform.position, ChasePathCallback);
    }

    private void ChasePathCallback(Vector2[] path, bool success) {
        if (!success) {
            path = new Vector2[1];
            path[0] = chaseTarget.position;
            Invoke("RetryChase", GameConf.RetryPathfindingTime);
        }
        ChaseState newState = new ChaseState(this, chaseTarget, path, alertPointDuration, chaseVisionRange);
        currentState = newState;
        CanSee = false;
    }

    public virtual void OnAlertDracula(Vector2 position) {
        Alert(position);
    }

    public virtual void OnAlertCorpse(Vector2 position, int instanceID) {
        if (!seenCorpses.Contains(instanceID)) {
            Alert(position);
            alertCorpseID = instanceID;
        }
    }

    protected override void OnSeenDracula(Collider2D collider) {
        Chase(collider.transform);
    }

    protected override void OnSeenCorpse(RaycastHit2D hitInfo, int instanceID) {
        if (!seenCorpses.Contains(instanceID)) {
            Alert(hitInfo.collider.transform.position);
            alertCorpseID = instanceID;
        }
    }

    public override void GoToPatrol() {
        currentState = patrolState;
        CanSee = true;
        running = false;
    }

    public void MarkCorpseAsSeen() {
        if (alertCorpseID != 0) {
            seenCorpses.Add(alertCorpseID);
            alertCorpseID = 0;
        }
    }

    public void Fire(Transform target) {
        DraculaController dracula = target.GetComponent<DraculaController>();
        characterController.Fire(dracula);
    }

    public IState CurrentState {
        get
        {
            return currentState;
        }
        set
        {
            currentState = value;
        }
    }

    public PatrolState PatrolState {
        get
        {
            return patrolState;
        }
    }

    public bool running {
        get { return characterController.running; }
        set { characterController.running = value; }
    }

    public bool CanSee {
        get { return canSee; }
        set { canSee = value; }
    }
}
