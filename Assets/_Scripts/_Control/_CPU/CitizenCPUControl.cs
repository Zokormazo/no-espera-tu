﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitizenCPUControl : CPUControl {
    // Variables exported to Unity Editor's Inspector
    [Header("Patrol State")]
    [Tooltip("Path")]
    [SerializeField]
    private PathDefinition patrolPath;
    [Header("Escape State")]
    [Tooltip("Escape point")]
    [SerializeField]
    private Transform escapePoint;
    [Header("Hidden State")]
    [Tooltip("Time to hide")]
    [SerializeField]
    private float timeToHide;

    // State Machine & States related variables
    private IState currentState;
    public PatrolState patrolState;

    // Components
    CitizenController citizenController;

    private new void Awake() {
        base.Awake();
        patrolState = new PatrolState(this, patrolPath);
    }

    private new void Start() {
        base.Start();
        currentState = patrolState;
        citizenController = GetComponent<CitizenController>();
    }

    private void Update() {
        if (canMove) {
            currentState.UpdateState();
        }
    }

    public void Escape() {
        if (escapePoint != null)
            PathRequestManager.RequestPath(transform.position, escapePoint.position, EscapePathCallback);
    }

    private void EscapePathCallback(Vector2[] path, bool success) {
        if (!success) {
            path = new Vector2[1];
            path[0] = escapePoint.position;
            Invoke("Escape", GameConf.RetryPathfindingTime);
        }

        if (success) {
            EscapeState newState = new EscapeState(this, path);
            currentState = newState;
        } else {
            Vector2[] myPath = new Vector2[1];
            myPath[0] = escapePoint.position;
            EscapeState newState = new EscapeState(this, myPath);
            currentState = newState;
        }
    }

    protected override void OnSeenDracula(Collider2D collider) {
        citizenController.Shout(ShoutSeen.Dracula, collider.transform.position);
        Escape();
    }

    protected override void OnSeenCorpse(RaycastHit2D hitInfo, int instanceID) {
        citizenController.Shout(ShoutSeen.Corpse, hitInfo.transform.position, instanceID);
    }

    public void Show(bool value) {
        citizenController.Show(value);
    }

    public PatrolState PatrolState {
        get
        {
            return patrolState;
        }
    }

    public IState CurrentState {
        get
        {
            return currentState;
        }
        set
        {
            currentState = value;
        }
    }

    public float TimeToHide {
        get
        {
            return timeToHide;
        }
    }

    public bool running {
        get { return characterController.running; }
        set { characterController.running = value; }
    }

    public override void GoToPatrol() {
        currentState = patrolState;
    }

    public void DoctorComing(DoctorCPUControl doctor) {
        Stop();
        citizenController.state = CharacterState.Immobilized;
    }

    protected override void CharacterController_OnStateChange(CharacterState state) {
        base.CharacterController_OnStateChange(state);
        switch(state) {
            case CharacterState.Escaping:
                citizenController.Shout(ShoutSeen.Dracula, transform.position);
                break;
        }
    }
}
