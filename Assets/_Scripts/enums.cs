﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum EnumDrawGizmos {
    Never,
    Selected,
    Always
}
