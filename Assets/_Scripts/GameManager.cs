﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public enum GameModes  {
    History,
    Level
}

public class GameManager : MonoBehaviour {

    private List<Score> _scores;
    private int _lastLevelSceneId;

    private Text _loadingText;
    private Image _loadingImage;
    private GameObject _loadingBar;
    private GameModes _gameMode = GameModes.History;
    private static GameManager instance;
    private AsyncOperation _loadingOperation;

    private void Awake() {
        instance = this;
        LevelManager.OnLevelEnd += LevelManager_OnLevelEnd;
        LevelManager.OnGameOver += LevelManager_OnGameOver;
        DontDestroyOnLoad(gameObject);
    }

    private void LevelManager_OnGameOver() {
        SceneManager.LoadScene("Dead");
    }

    private void LevelManager_OnLevelEnd(bool success, Score score) {
        if (success) {
            _scores.Add(score);
            SaveScore();
            if (_gameMode == GameModes.History) {
                _lastLevelSceneId = SceneManager.GetActiveScene().buildIndex;
                int currentLevel = (_lastLevelSceneId - 8) / 2 + 1;
                if (currentLevel > PlayerPrefs.GetInt("Completed Levels"))
                    PlayerPrefs.SetInt("Completed Levels", currentLevel);
                PlayerPrefs.SetInt("Current Level", currentLevel);
            }
            instance.StartCoroutine(instance.LoadScene());
            instance.StartCoroutine(instance.MonitorLoading());
            instance._loadingBar.SetActive(true);
        }
    }

    private void SaveScore() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/currentscore.dat");

        bf.Serialize(file, _scores);
        file.Close();
    }

    public void LoadScore() {
        if (File.Exists(Application.persistentDataPath + "/currentscore.dat")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/currentscore.dat", FileMode.Open);
            _scores = (List<Score>)bf.Deserialize(file);
            file.Close();
        }
    }

    private IEnumerator LoadScene() {
        instance._loadingOperation = SceneManager.LoadSceneAsync("NivelTerminado");
        yield return instance._loadingOperation;
    }

    private IEnumerator MonitorLoading() {
        while (instance._loadingOperation.progress < 0.9f) {
            if (instance._loadingText != null)
                instance._loadingText.text = string.Format("{0}%", Mathf.Round(instance._loadingOperation.progress * 100f));
            if (instance._loadingImage != null)
                instance._loadingImage.fillAmount = instance._loadingOperation.progress;
            yield return null;
        }
    }

    private void Start() {
        _scores = new List<Score>();
        SceneManager.LoadScene(1);
    }

    public static Text loadingText {
        get { return instance._loadingText; }
        set { if (instance != null) instance._loadingText = value; }
    }

    public static Image loadingImage {
        get { return instance._loadingImage; }
        set { if (instance != null) instance._loadingImage = value; }
    }

    public static GameObject loadingBar {
        get { return instance._loadingBar; }
        set { if (instance != null) instance._loadingBar = value; }
    }

    public static Score LastScore {
        get { return instance._scores[instance._scores.Count - 1]; }
    }

    public static GameModes GameMode {
        get { return instance._gameMode; }
        set { if (instance) instance._gameMode = value; }
    }

    public static int totalScore {
        get
        {
            int sum = 0;
            foreach(Score score in instance._scores) {
                sum += score.score;
            }
            return sum;
        }
    }

    public static int nextScene {
        get { return (instance._gameMode == GameModes.History) ? instance._lastLevelSceneId + 1 : 7; }
    }
}