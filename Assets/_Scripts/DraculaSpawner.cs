﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraculaSpawner : MonoBehaviour {

    [SerializeField]
    private GameObject _dracula;
    [SerializeField]
    private float _timeToWait = 0.5f;
    private float _runningTime;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (_runningTime < _timeToWait)
            _runningTime += Time.deltaTime;
        else {
            Instantiate(_dracula, transform.position, Quaternion.identity);
            this.enabled = false;
        }
    }
}
