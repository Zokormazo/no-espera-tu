﻿using System;

[Serializable]
public class HighScore : IComparable<HighScore> {
    private string _playerName;
    private int _score;

    public HighScore(string _playerName, int _score) {
        this._playerName = _playerName;
        this._score = _score;
    }

    public string playerName {
        get { return _playerName; }
    }

    public int score {
        get { return _score; }
    }

    public int CompareTo(HighScore that) {
        if (that == null)
            return 1;
        return that._score.CompareTo(this._score);
    }
}
