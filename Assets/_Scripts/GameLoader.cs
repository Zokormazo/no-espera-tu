﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class GameLoader : MonoBehaviour
{

    //Declaramos una variable que nos indica el proceso de carga de la escena en segundo plano
    private AsyncOperation _gameLoadingOperation;

    //Declaramos la imagen que se rellenará con el porcentaje del nivel cargado
    public Image _loadingImage;
    public Text _cantloadingImage;
    public Button _startButton;
    public string _levelToLoad;

    private float _totporc;

    private void Start() {
        LoadGame();
        _startButton.gameObject.SetActive(false);
    }

    private void LoadGame()
    {
 
        //Llamamos a la corutina para iniciar la carga de manera asíncrona
        StartCoroutine(LoadGameAsync());
    }

    //Creamos una Corutina para poder cargar el nivel en segundo plano
    private IEnumerator LoadGameAsync()
    {

        //Iniciamos el proceso de carga de manera asíncrona
        _gameLoadingOperation = SceneManager.LoadSceneAsync(_levelToLoad);
        _gameLoadingOperation.allowSceneActivation = false;
        yield return _gameLoadingOperation;
    }

    private void Update()
    {

        //Nos aseguramos de que la variable que carga el nivel tiene valor
        if (_gameLoadingOperation != null)
        {
            if (_gameLoadingOperation.progress < 0.9f) {
                //Rellenamos la imagen con la cantidad de porcentaje cargado del nivel
                _loadingImage.fillAmount = _gameLoadingOperation.progress;
                _totporc = Mathf.Round(_gameLoadingOperation.progress * 100f);
                _cantloadingImage.text = _totporc.ToString() + " %";
            } else {
                _cantloadingImage.text = "100 %";
                _loadingImage.fillAmount = 1.0f;
                _startButton.gameObject.SetActive(true);
            }
        }
    }

    public void StartLevel() {
        _gameLoadingOperation.allowSceneActivation = true;
    }
}
