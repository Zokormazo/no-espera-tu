﻿using System;
using UnityEngine;

public class Node : IHeapItem<Node> {
    public bool walkable;
    public Vector2 worldPosition;
    public int gridX;
    public int gridY;

    public int gCost;
    public int hCost;

    public Node parent;

    private int _heapIndex;

    // Constructor method
    public Node(bool _walkable, Vector2 _worldPosition, int _gridX, int _gridY) {
        walkable = _walkable;
        worldPosition = _worldPosition;

        gridX = _gridX;
        gridY = _gridY;
    }

    public int fCost {
        get { return hCost + gCost; }
    }

    public int HeapIndex {
        get { return _heapIndex; }
        set { _heapIndex = value; }
    }

    public int CompareTo(Node other) {
        int compare = fCost.CompareTo(other.fCost);
        if (compare == 0)
            compare = hCost.CompareTo(other.hCost);
        return -compare; 
    }
}
