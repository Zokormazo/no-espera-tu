﻿using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

    // Variables exported to Unity Editor's Inspector
    [Header("Grid")]
    [Tooltip("Area that the grid will cover")]
    [SerializeField]
    private Vector2 _gridWorldSize;
    [Tooltip("Radius of each node")]
    [SerializeField]
    private float _nodeRadius;
    [Tooltip("Radius multiplicator for grid creation")]
    [SerializeField]
    private float _nodeRadiusMultiplicator = 1.3f;
    [Header("Gizmos")]
    [SerializeField]
    private bool _displayGridGizmos;

    private float _nodeDiameter;

    private Node[,] _grid;

    private LayerMask _unwalkableMask;

    private int _gridSizeX;
    private int _gridSizeY;

    private void Awake() {
        _unwalkableMask = LayerMask.GetMask("TransparentObjects", "UnwalkableObjects");
        _nodeDiameter = _nodeRadius * 2;
        _gridSizeX = Mathf.RoundToInt(_gridWorldSize.x / _nodeDiameter);
        _gridSizeY = Mathf.RoundToInt(_gridWorldSize.y / _nodeDiameter);
        CreateGrid();
    }

    private void CreateGrid() {
        _grid = new Node[_gridSizeX, _gridSizeY];
        Vector2 worldBottomLeft = (Vector2)transform.position - Vector2.right * _gridWorldSize.x / 2 - Vector2.up * _gridWorldSize.y / 2;
        float overlapRadius = _nodeRadius * _nodeRadiusMultiplicator;
        for (int x = 0; x < _gridSizeX; x++) {
            for (int y = 0; y < _gridSizeY; y++) {
                Vector2 worldPoint = worldBottomLeft + Vector2.right * (x * _nodeDiameter + _nodeRadius) + Vector2.up * (y * _nodeDiameter + _nodeRadius);
                bool walkable = Physics2D.OverlapCircle(worldPoint, overlapRadius, _unwalkableMask) == null;
                _grid[x, y] = new Node(walkable, worldPoint, x, y);
            }
        }
    }

    public int MaxSize {
        get { return _gridSizeX * _gridSizeY; }
    }

    public Node NodeFromWorldPoint(Vector2 worldPosition) {
        float percentX = (worldPosition.x - transform.position.x) / _gridWorldSize.x + 0.5f - (_nodeRadius / _gridWorldSize.x);
        float percentY = (worldPosition.y - transform.position.y) / _gridWorldSize.y + 0.5f - (_nodeRadius / _gridWorldSize.y);

        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((_gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((_gridSizeY - 1) * percentY);

        return _grid[x, y];
    }

    public List<Node> GetNeighbours(Node node) {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
            for (int y = -1; y <= 1; y++) {
                if (x == 0 && y == 0)
                    continue;

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if (checkX >= 0 && checkX < _gridSizeX && checkY >= 0 && checkY < _gridSizeY) {
                    neighbours.Add(_grid[checkX, checkY]);
                }
            }

        return neighbours;
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireCube(transform.position, new Vector3(_gridWorldSize.x, _gridWorldSize.y, 1f));

        if (_grid != null && _displayGridGizmos) {
            foreach(Node n in _grid) {
                Gizmos.color = (n.walkable) ? Color.white : Color.red;
                Gizmos.DrawCube(n.worldPosition, Vector3.one * (_nodeDiameter - .05f));
            }
        }
    }
}
