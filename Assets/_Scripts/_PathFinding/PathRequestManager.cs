﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PathFinding))]
public class PathRequestManager : MonoBehaviour {

    private Queue<PathRequest> _pathRequestQueue = new Queue<PathRequest>();
    private PathRequest _currentPathRequest;

    private static PathRequestManager _instance;
    private PathFinding _pathfinding;

    private bool _isProcessingPath;

    private void Awake() {
        _instance = this;
        _pathfinding = GetComponent<PathFinding>();
    }

    public static void RequestPath(Vector2 pathStart, Vector2 pathEnd, Action<Vector2[], bool> callback) {
        PathRequest newRequest = new PathRequest(pathStart, pathEnd, callback);
        _instance._pathRequestQueue.Enqueue(newRequest);
        _instance.TryProcessNext();
    }

    public void FinishedProcessingPath(Vector2[] path, bool success) {
        _currentPathRequest.callback(path, success);
        _isProcessingPath = false;
        TryProcessNext();
    }


    private void TryProcessNext() {
        if (!_isProcessingPath && _pathRequestQueue.Count > 0 ) {
            _currentPathRequest = _pathRequestQueue.Dequeue();
            _isProcessingPath = true;
            _pathfinding.StartFindPath(_currentPathRequest.pathStart, _currentPathRequest.pathEnd);
        }
    }

    struct PathRequest
    {
        public Vector2 pathStart;
        public Vector2 pathEnd;
        public Action<Vector2[], bool> callback;

        public PathRequest(Vector2 _start, Vector2 _end, Action<Vector2[], bool> _callback) {
            pathStart = _start;
            pathEnd = _end;
            callback = _callback;
        }
    }
}
