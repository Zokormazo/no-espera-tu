﻿/* 
 * PathDefinition.cs
 * Generates path with it's childrens positions
  */
using System.Collections.Generic;
using UnityEngine;

public class PathDefinition : MonoBehaviour {

    // Variables exported to Unity Editor's Inspector
    [Header("Gizmos")]
    [Tooltip("When to draw gizmos on Editor")]
    [SerializeField]
    private EnumDrawGizmos drawGizmos;

    // Waypoints
    private List<Vector2> _waypoints = new List<Vector2>();
    private bool _isGenerated;

    // Draw Gizmos unless object is collapsed
    private void OnDrawGizmos() {
        if (drawGizmos == EnumDrawGizmos.Always) {
            GenerateWaypoints();
            DrawPath();
        }
    }

    // Draw Gizmos when object is selected
    private void OnDrawGizmosSelected() {
        if (drawGizmos != EnumDrawGizmos.Never) {
            GenerateWaypoints();
            DrawPath();
        }
    }

    // Early Initialization
    private void Awake() {
        GenerateWaypoints();
    }

    // Generate the waypoints list with children's positions
    private void GenerateWaypoints() {
        if (!_isGenerated) // Generate waypoints only is not generated yet.
            foreach (Transform child in transform) // Loop on children transform
                _waypoints.Add(child.position); // Add their position to the waypoint's list
        _isGenerated = true; // Mark waypoints as generated
    }

    // FIXIT: Move it to an outside lib.
    private void DrawPath() {
        int i = 1;
        foreach(Vector2 waypoint in _waypoints) {
            Vector2 dest;
            if (i < _waypoints.Count)
                dest = _waypoints[i];
            else
                dest = _waypoints[0];
            Gizmos.DrawLine(waypoint, dest);
            i++;
        }

        Gizmos.color = Color.blue;
        foreach(Vector2 waypoint in _waypoints) {
            Gizmos.DrawCube(waypoint, 0.2f * Vector3.one);
        }
    }

    // Return waypoints array.
    public Vector2[] waypoints {
        get {
            GenerateWaypoints();
            return _waypoints.ToArray();
        }
    }
}
