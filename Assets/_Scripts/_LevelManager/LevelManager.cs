﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    [Header("Objetives")]
    [Tooltip("Needed blood")]
    [SerializeField]
    private float _neededBlood;
    [Tooltip("Initial blood")]
    [SerializeField]
    private float _initialBlood;

    [Header("Clock")]
    [SerializeField]
    private Clock _clock;

    [Header("Level Ending")]
    [Tooltip("Move Dracula to end position by CPU")]
    [SerializeField]
    private bool _moveDraculaOnLevelEnd;
    [Tooltip("Dracula gameObject")]
    [SerializeField]
    private GameObject _dracula;
    [Tooltip("Dracula's level end position")]
    [SerializeField]
    private Transform _draculaEndPoint;

    public delegate void LevelEnd(bool success, Score score);
    public static event LevelEnd OnLevelEnd;
    public delegate void GameOver();
    public static event GameOver OnGameOver;

    // Score related
    private float _collectedBlood;
    private int _killedCitizenCount;

    protected void Awake() {
        LevelExitPoint.OnReachedExitPoint += LevelExitPoint_OnReachedExitPoint;
        DraculaController.OnBloodChange += DraculaController_OnBloodChange;
        DraculaController.OnKillCitizen += DraculaController_OnKillCitizen;
        DraculaController.OnDraculaDead += DraculaController_OnDraculaDead;
        EndLevelOnKill.OnKill += EndLevelOnKill_OnKill; 
        _clock.OnTimeEnd += _clock_OnTimeEnd;
    }

    // Use this for initialization
    private void Start () {
        _collectedBlood = _initialBlood;
        _clock.Start();
	}

    // Event Callbacks

    private void DraculaController_OnBloodChange(float delta, float total, float max) {
        _collectedBlood += delta;
    }

    private void DraculaController_OnKillCitizen() {
        _killedCitizenCount++;
    }

    private void LevelExitPoint_OnReachedExitPoint() {
        if (_collectedBlood >= _neededBlood) {
            if (_moveDraculaOnLevelEnd) {
                UserControl userControl = _dracula.GetComponent<UserControl>();
                MoveCharacterToPoint moveToPoint = _dracula.GetComponent<MoveCharacterToPoint>();
                userControl.enabled = false;
                moveToPoint.point = _draculaEndPoint.position;
                moveToPoint.enabled = true;
                Invoke("DelayedLevelEnd", 8f);
            } else if (OnLevelEnd != null)
                OnLevelEnd(_collectedBlood >= _neededBlood, new Score(_collectedBlood, Time.timeSinceLevelLoad, _killedCitizenCount));
        }
    }

    private void DelayedLevelEnd() {
        if (OnLevelEnd != null)
            OnLevelEnd(_collectedBlood >= _neededBlood, new Score(_collectedBlood, Time.timeSinceLevelLoad, _killedCitizenCount));
    }

    private void EndLevelOnKill_OnKill() {
        if (OnLevelEnd != null)
            OnLevelEnd(true, new Score(_collectedBlood, Time.timeSinceLevelLoad, _killedCitizenCount));
    }

    private void DraculaController_OnDraculaDead() {
        if (OnGameOver != null)
            OnGameOver();
    }

    private void _clock_OnTimeEnd() {
        if (OnGameOver != null)
            OnGameOver();
    }

    // Update is called once per frame
    private void Update () {
        _clock.Update();
	}
}
