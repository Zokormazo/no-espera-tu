﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

[Serializable]
public class Clock {
    [Header("Configuration")]
    [Tooltip("Sunset Time, in hours")]
    [SerializeField]
    private int _sunsetHour;
    [Tooltip("Dawn Time, in hours")]
    [SerializeField]
    private int _dawnHour;
    [Tooltip("Real-time duration for night, in seconds")]
    [SerializeField]
    private float _nightDuration;
    [Tooltip("Dawn transition time")]
    [SerializeField]
    private float _dawnTransitionDuration = 8f;

    [Header("UI")]
    [SerializeField]
    private Text _clockText;

    [Header("Color Correction")]
    [SerializeField]
    private float _finalSaturation = 1.0f;
    private float _finalVignette = 0.0f;

    // Clock related variables
    private int _gameTimeNightDuration;
    private float _currentTime;
    private float _timeRatio;

    // Color correction related variables
    private ColorCorrectionCurves _colorCorrectionCurves;
    private VignetteAndChromaticAberration _vignetteAndChromaticAberration;
    private float _saturationDPS;
    private float _vignetteDPS;

    private bool _countdownRunning;

    // Events
    public delegate void TimeEnd();
    public event TimeEnd OnTimeEnd;

    public void Start() {
        if (_sunsetHour > _dawnHour)
            _gameTimeNightDuration = (24 - _sunsetHour + _dawnHour) * 60;
        else
            _gameTimeNightDuration = (_dawnHour - _sunsetHour) * 60;
        _timeRatio = _gameTimeNightDuration / _nightDuration;

        _colorCorrectionCurves = Camera.main.GetComponent<ColorCorrectionCurves>();
        _vignetteAndChromaticAberration = Camera.main.GetComponent<VignetteAndChromaticAberration>();
        _saturationDPS = (_finalSaturation - _colorCorrectionCurves.saturation) / _dawnTransitionDuration;
        _vignetteDPS = (_finalVignette - _vignetteAndChromaticAberration.intensity) / _dawnTransitionDuration;
    }

    public void Update() {
        if (_currentTime < _gameTimeNightDuration) {
            _currentTime += Time.deltaTime * _timeRatio;
            RenderUI();
            if (_currentTime + _dawnTransitionDuration > _gameTimeNightDuration) {
                if (!_countdownRunning) {
                    MessageManager.ShowMessage("¡Está a punto de amanecer!");
                    _countdownRunning = true;
                }
                _colorCorrectionCurves.saturation += _saturationDPS * Time.deltaTime;
                _colorCorrectionCurves.saturation = Mathf.Clamp01(_colorCorrectionCurves.saturation);
                _vignetteAndChromaticAberration.intensity += _vignetteDPS * Time.deltaTime;
                _vignetteAndChromaticAberration.intensity = Mathf.Clamp01(_vignetteAndChromaticAberration.intensity);
            }
        }
        else if (OnTimeEnd != null)
            OnTimeEnd();
    }

    private void RenderUI() {
        if (_clockText) {
            int hours = ((int)(_currentTime / 60) + _sunsetHour) % 24;
            int minutes = ((int)_currentTime) % 60;
            _clockText.text = string.Format("{0:2}:{1:2}", hours.ToString("D2"), minutes.ToString("D2"));
        }
    }
}
