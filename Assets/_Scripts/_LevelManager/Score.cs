﻿using System;

[Serializable]
public class Score {

    private float _collectedBlood;
    private float _totalTime;
    private float _kills;

    public int score {
        get { return (int)(collectedBlood + kills * 100 + 1000 - totalTime);  }
    }

    public Score(float _collectedBlood, float _totalTime, float _kills) {
        this._collectedBlood = _collectedBlood;
        this._totalTime = _totalTime;
        this._kills = _kills;
    }

    public float collectedBlood {
        get { return _collectedBlood; }
    }

    public float totalTime {
        get { return _totalTime; }
    }

    public float kills {
        get { return _kills; }
    }
}
