﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevelOnKill : MonoBehaviour {

    public delegate void Kill();
    public static event Kill OnKill;

    private MyCharacterController _characterController;

	// Use this for initialization
	private void Start () {
        _characterController = GetComponent<MyCharacterController>();
        _characterController.OnStateChange += _characterController_OnStateChange;
	}

    private void OnDestroy() {
        OnKill = null;
    }

    private void _characterController_OnStateChange(CharacterState state) {
        if (state == CharacterState.Dead) {
            if (OnKill != null)
                OnKill();
        }
    }
}
