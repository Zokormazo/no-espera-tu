﻿using UnityEngine;

public class LevelExitPoint : MonoBehaviour {

    // Static event
    public delegate void ReachedExitPoint();
    public static event ReachedExitPoint OnReachedExitPoint;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Dracula" && OnReachedExitPoint != null)
            OnReachedExitPoint();
    }

    private void OnDestroy() {
        OnReachedExitPoint = null;
    }
}
