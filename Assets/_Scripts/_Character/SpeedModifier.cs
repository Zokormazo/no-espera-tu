﻿/* 
 * SpeedMofifier.cs
 * Defines the speed modifier
 */

public enum SpeedModifierTypes {
    Shoot,
    HolyCross,
    Carrying,
    Flying
}

public class SpeedModifier {
    private float _value;
    private float _duration;
    private float _timeLeft;

    public SpeedModifier(float _value, float _duration = 0) {
        this._value = _value;
        this._duration = _duration;
        this._timeLeft = _duration;
    }

    public float value {
        get { return _value; }
    }

    public float duration {
        get { return _duration; }
    }

    public float timeLeft {
        get { return _timeLeft; }
        set { _timeLeft = value; }
    }
}
