﻿using UnityEngine;

public enum CopAnimationActions
{
    Parado = 0,
    Andando = 1,
}

[RequireComponent(typeof(AudioSource))]
public class CopController : MyCharacterController {

    // Variables exported to Unity Editor's Inspector
    [Header("Shooting")]
    [Tooltip("Weapon Range")]
    [SerializeField]
    protected float _weaponRange;
    [Tooltip("Weapon Debuff Power")]
    [SerializeField]
    protected float _weaponPower;
    [Tooltip("Weapon Damage Power")]
    [SerializeField]
    protected float _weaponDamage = 5.0f;
    [Tooltip("Debuff duration")]
    [SerializeField]
    protected float _weaponDuration;
    [Tooltip("Shooting interval")]
    [SerializeField]
    protected float _weaponInterval;
    [Tooltip("Shooting accuracy")]
    [SerializeField]
    protected float _weaponAccuracy;

    [Header("Sound Effects")]
    [Tooltip("Shoot sound effect")]
    [SerializeField]
    protected AudioClip _weaponSFX;

    // Components
    protected AudioSource _copAudioSource;

    // Shooting related variables
    protected float _lastShootTime;

	// Use this for initialization
	private new void Start () {
        base.Start();
        _copAudioSource = GetComponent<AudioSource>(); // Get cop's Audiosource for later usage
    }

    // Called every frame that cop has a potential shoot.
    public override void Fire(DraculaController dracula) {
        // If we are not on the weapon interval time & dracula is on weapon range
        if (dracula.state != CharacterState.Flying && Time.time - _weaponInterval > _lastShootTime && Vector2.Distance(transform.position, dracula.transform.position) < _weaponRange) {
            if (_weaponSFX)
                _copAudioSource.PlayOneShot(_weaponSFX); // Play SFX audio
            _characterAnimator.SetTrigger("Fire"); // Play Animation
            _lastShootTime = Time.time; // Store last shoot time

            if (Random.value <= _weaponAccuracy) { // We have a hit
                SpeedModifier modifier = new SpeedModifier(-_weaponPower, _weaponDuration); // Create speed modifier
                dracula.AddSpeedModifier(SpeedModifierTypes.Shoot, modifier); // Apply speed modifier to dracula
                dracula.DrainBlood(_weaponDamage); // Apply weapon damage to dracula
            }
        }
    }
}
