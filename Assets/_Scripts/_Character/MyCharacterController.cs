﻿/* 
 * CharacterController.cs
 * All game character's parent class.
 */
using System.Collections.Generic;
using UnityEngine;

// States for all Characters
public enum CharacterState {
    Normal,
    Immobilized,
    Blinded,
    Stunned,
    Carrying,
    Escaping,
    Flying,
    Dead
}

// Animation actions common to all characters
public enum CharacterAnimationActions {
    Parado = 0,
    Andando = 1
}

public enum CharacterLookingAt
{
    Right,
    Left
}

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
abstract public class MyCharacterController : MonoBehaviour {

    // Variables exported to Unity Editor's Inspector
    [Header("State")]
    [Tooltip("Initial state of the character")]
    [SerializeField]
    private CharacterState _initialCharacterState;
    [Header("Movement configuration")]
    [Tooltip("Base speed multiplicator")]
    [SerializeField]
    private float _baseSpeedMultiplicator = 0.5f;
    [Tooltip("Running speed increment")]
    [SerializeField]
    private float _runningSpeedIncrement = 0.3f;
    [Tooltip("Minimun movement multiplicator")]
    [SerializeField]
    private float _minimunSpeedMultiplicator = 0.1f;

    [Tooltip("Melee considered distance")]
    [SerializeField]
    protected float _meleeDistance = 0.5f;

    [Header("GFX")]
    [Tooltip("Inverse X flipping")]
    [SerializeField]
    private bool _inverseFlipX;

    // State
    private CharacterState _currentCharacterState;
    protected CharacterState _previousCharacterState;

    // Events
    public delegate void StateChangeHanlder(CharacterState state);
    public event StateChangeHanlder OnStateChange;

    // Movement
    private bool _isMoving;
    private bool _isRunning;
    private bool _canMove;
    private Dictionary<SpeedModifierTypes, SpeedModifier> _speedModifiers;
    private CharacterLookingAt _lookingAt;

    // Components
    protected Rigidbody2D _characterRigidbody2D;
    protected BoxCollider2D _characterBoxCollider2D;
    protected SpriteRenderer _characterSpriteRenderer;
    protected Animator _characterAnimator;

    /*
     * Methods called by Unity
     */

    // Early Initialization
    private void Awake() {
        _canMove = true;
        _characterAnimator = GetComponent<Animator>(); // Get character's Animator for later usage
        state = _initialCharacterState; // Initialize character state
    }

    // Initialization method
    protected void Start () {
        _characterRigidbody2D = GetComponent<Rigidbody2D>(); // Get character's Rigidbody2D for later usage
        _characterBoxCollider2D = GetComponent<BoxCollider2D>(); // Get character's BoxCollider2D for later usage
        _characterSpriteRenderer = GetComponent<SpriteRenderer>(); // Get character's SpriteRenderer for later usage
        _characterRigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation; // Avoid rotation of Characters
        _speedModifiers = new Dictionary<SpeedModifierTypes, SpeedModifier>();
    }

    // Main Loop
    protected void Update() {
        if (_speedModifiers != null && _speedModifiers.Count > 0) { // We have at least one speed modifier
            List<SpeedModifierTypes> keysToRemove = new List<SpeedModifierTypes>(); // Initialize list to store the key of the modifiers that must be removed later
            foreach( KeyValuePair<SpeedModifierTypes, SpeedModifier> kvp in _speedModifiers) { // Iterate over all the speed modifiers
                if (kvp.Value.duration > 0) { // Has duration
                    if (kvp.Value.timeLeft > 0) // Remaining time
                        kvp.Value.timeLeft -= Time.deltaTime; // Decrement time left
                    else // Time ended. The modifier must be removed
                        keysToRemove.Add(kvp.Key); // Store modifiers key to remove later
                }
            }
            foreach(SpeedModifierTypes key in keysToRemove) { // Iterate over keys to be removed
                _speedModifiers.Remove(key); // Remove the speed modifier
            }
        }
    }

    /*
     * Private properties
     */

    // Returns current speed multiplicator value
    protected float speedMultiplicator {
        get
        {
            float multiplicator = _baseSpeedMultiplicator; // Start with base multiplicator
            if (_isRunning) { // We are actually running
                multiplicator += _runningSpeedIncrement; // Increment multiplicator with speed increment
            }
            foreach (KeyValuePair<SpeedModifierTypes, SpeedModifier> kvp in _speedModifiers) { // Iterate over speed modifiers
                multiplicator += kvp.Value.value; // Increment multiplicator with modifier's value
            }
            return Mathf.Max(multiplicator, _minimunSpeedMultiplicator); // Return the calculated modifier or the minimum value if calculated modifier is less than minimum movement velocity
        }
    }

    /*
     * Public action methods
     */

    // Move character on given vector's direction. If vector's magnitude is bigger than one the vector is normalized
    public void Move(Vector2 velocity) {
        if (velocity.x == 0 && velocity.y == 0) { // We are not moving
            if (_isMoving) { // We were moving
                if (_characterAnimator.GetInteger("Accion") == (int)CharacterAnimationActions.Andando) // The animation state is 'Andando'
                    _characterAnimator.SetInteger("Accion", (int)CharacterAnimationActions.Parado); // Set 'Parado' animation
                _isMoving = false; // We are not moving
            }
            _characterRigidbody2D.velocity = Vector2.zero; // Stop the rigidbody.
        } else { // We have a non zero vector to move
            if (!_isMoving) { // We aren't moving yet
                _characterAnimator.SetInteger("Accion", (int)CharacterAnimationActions.Andando); // Set 'Andando' animation
                _isMoving = true; // We are moving
            }
            if (velocity.magnitude > 1) // Normalize vector if needed
                velocity.Normalize();
            if (velocity.x != 0 && alive) { // Of we are moving on x axis
                bool flipX = velocity.x > 0; // Calculate flipX value
                _lookingAt = (flipX) ? CharacterLookingAt.Right : CharacterLookingAt.Left;
                if (_inverseFlipX) // Inverse flipX if needed
                    flipX = !flipX;
                _characterSpriteRenderer.flipX = flipX; // Set flipX value on SpriteRenderer so the sprite looks towards our x movement direction

            }
            if (_canMove) // We can move
                _characterRigidbody2D.velocity = velocity * speedMultiplicator; // Move the rigidbody
        }
    }

    // Stop moving
    public void Stop() {
        Move(Vector2.zero);
    }

    // Add a new Speed modifier of specified type to the character. Replace the current one if already exists a speed modifier of given type
    public void AddSpeedModifier(SpeedModifierTypes newType, SpeedModifier newModifier) {
        if (_speedModifiers.ContainsKey(newType)) // There is an speed modifier of newType already.
            _speedModifiers.Remove(newType); // Remove the current speed modifier
        _speedModifiers.Add(newType, newModifier); // Add the new speed modifier
    }

    // Remove the speed modifier of specified type from the character.
    public void RemoveSpeedModifier(SpeedModifierTypes type) {
        if (_speedModifiers.ContainsKey(type)) // There is an speed modifier of type
            _speedModifiers.Remove(type); // Remove it
    }

    public void PruneSpeedModifiers() {
        _speedModifiers.Clear();
    }

    public int CountSpeedModifiers() {
        return _speedModifiers.Count;
    }

    // Try to Attack target
    virtual public void Fire(DraculaController target) { }

    /*
     * Public properties
     */

        // Get/Set character's state
    public virtual CharacterState state {
        get { return _currentCharacterState; }
        set
        {
            _previousCharacterState = _currentCharacterState;
            switch (_previousCharacterState) {
                case CharacterState.Carrying:
                    RemoveSpeedModifier(SpeedModifierTypes.Carrying);
                    break;
                case CharacterState.Flying:
                    RemoveSpeedModifier(SpeedModifierTypes.Flying);
                    gameObject.layer = 0;
                    break;
            }
            _currentCharacterState = value;
            if (OnStateChange != null)
                OnStateChange(value);
            switch (value) {
                case CharacterState.Normal:
                    _canMove = true;
                    break;
                case CharacterState.Escaping:
                    _canMove = true;
                    break;
                case CharacterState.Dead:
                    _canMove = false;
                    Stop();
                    break;
                case CharacterState.Stunned:
                    _canMove = false;
                    Stop();
                    break;
            }
        }
    }

    // Returns wether we're alive or not
    public bool alive {
        get { return _currentCharacterState != CharacterState.Dead; } // Alive unless we are Dead ;)
    }

    // Get/Set running state
    public bool running {
        get { return _isRunning; }
        set { _isRunning = value; }
    }

    public float meleeDistance {
        get { return _meleeDistance; }
    }

    public CharacterLookingAt lookingAt {
        get { return _lookingAt; }
    }
}
