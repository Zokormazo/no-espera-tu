﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCharacterToPoint : MonoBehaviour {

    private Vector2 _point;
    private Vector2[] _waypoints;
    private int _currentWaypoint;
    private MyCharacterController _control;
    private Rigidbody2D _rigidbody;
    private TranscisionAnimator _animator;

    private void Start() {
        _animator = FindObjectOfType<TranscisionAnimator>();
    }

	// Update is called once per frame
	void Update () {
		if (_waypoints != null ) {
            if (_currentWaypoint < _waypoints.Length) { // We are not on the last waypoint
                Vector2 moveDirection = _waypoints[_currentWaypoint] - (Vector2)transform.position; // Calculate movement direction
                if (moveDirection.magnitude < GameConf.WaypointReachedConsideredDistance) // We reached a waypoint
                    _currentWaypoint++; // Set next waypoint as current waypoint
                else // We haven't reached next waypoint yet
                    _control.Move(moveDirection); // Move on calculated direction
            }
            else {
                Vector2 moveDirection = _point - (Vector2)transform.position;
                if (moveDirection.magnitude < GameConf.WaypointReachedConsideredDistance) {
                    transform.position = _point;
                    _control.Stop();
                    _animator.enabled = true;
                    _animator.vuelta_1 = true;
                    Invoke("DelayedDisable", 0.5f);
                } else {
                    _control.Move(moveDirection);
                }
            }
        }
	}

    private void DelayedDisable() {
        gameObject.SetActive(false);
    }

    private void OnEnable() {
        _waypoints = null;
        _currentWaypoint = 0;
        _control = GetComponent<MyCharacterController>();
        _control.PruneSpeedModifiers();
        PathRequestManager.RequestPath(transform.position, point, MoveToPointPathCallback);
    }

    private void MoveToPointPathCallback(Vector2[] path, bool success) {
        if (success) {
            _waypoints = path;
        } else {
            _waypoints = new Vector2[1];
            _waypoints[0] = _point;
        }
    }

    public Vector3 point {
        get { return _point; }
        set {
            _point = value;
        }
    }
}
