﻿/* 
 * DraculaController.cs
 * Dracula's main controller class.
 */
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public enum DraculaAnimationStates {
    Normal   = 0,
    Cegado   = 1,
    Atacando = 2,
    Cargando = 3,
    Volando = 4
}

public enum DraculaAnimationActions {
    Parado = 0,
    Andando = 1,
    Mordiendo = 2,
    Muerto = 3
}

[RequireComponent(typeof(AudioSource))]
public class DraculaController : MyCharacterController {

    // Variables exported to Unity Editor's Inspector
    [Header("Blood")]
    [Tooltip("Max Blood Quantity")]
    [SerializeField]
    private float _maxBloodQuantity;
    [Tooltip("Initial Blood Quantity")]
    [SerializeField]
    private float _initialBloodQuantity;
    [Tooltip("Blood Draining Rate (BPS)")]
    [SerializeField]
    private int _bloodPerSecond;
    [Tooltip("Minimum blood to be able to fly")]
    [SerializeField]
    private float _flyingMinimumBlood = 50f;

    [Header("Audio")]
    [Tooltip("SFX for bit start")]
    [SerializeField]
    private AudioClip _bitAudioClip; // Sounds once when Dracula bits a Citizen
    [Tooltip("SFX for each of draining pulses")]
    [SerializeField]
    private AudioClip _drainAudioClip; // Sounds every time Dracula drains a shot
    [Tooltip("SFX for dead")]
    [SerializeField]
    private AudioClip _deadAudioClip; // Sounds when Dracula is killed

    [Tooltip("Enable/Disable carrying")]
    [SerializeField]
    bool _canCarry = true;

    [Header("Carry Corpses")]
    [Tooltip("Movement debuff power on carrying corpses")]
    [SerializeField]
    private float _carryingDebuffPower;

    // Components
    private AudioSource _audioSource;

    // Blood related variables
    private float _currentBloodQuantity; // Current blood quantity

    // Bitting related variables
    private CitizenController _biteTarget; // Stores potential biteTarget when a Citizen is next to Dracula. null otherwise
    private bool _isDraining; // Is dracula draining target?
    private IEnumerator _runningDrainCoroutine; // Stores draining coroutine
    private IEnumerator _runningFlyingCoroutine;

    // Carrying related variables
    [SerializeField]
    private CitizenController _carryingTarget;

    // Events
    public delegate void BloodChange(float delta, float absolute, float max);
    public static event BloodChange OnBloodChange;
    public delegate void KillCitizen();
    public static event KillCitizen OnKillCitizen;
    public delegate void DraculaDead();
    public static event DraculaDead OnDraculaDead;

    // Use this for initialization
    private new void Start() {
        base.Start(); // Exec parent's Start()
        _currentBloodQuantity = _initialBloodQuantity; // Initialize current blood quantity
        if (OnBloodChange != null)
            OnBloodChange(_initialBloodQuantity, _currentBloodQuantity, _maxBloodQuantity);
        _audioSource = GetComponent<AudioSource>(); // Get Draculas's AudioSource for later usage
    }

    private void OnDestroy() {
        OnBloodChange = null;
        OnKillCitizen = null;
        OnDraculaDead = null;
    }

    private bool IsColliderInFrontOfUs(Collider2D collider) {
        return (lookingAt == CharacterLookingAt.Right) ?
            collider.transform.position.x >= transform.position.x - _characterBoxCollider2D.size.x / 2 :
            collider.transform.position.x <= transform.position.x + _characterBoxCollider2D.size.x /2;
    }

    public void Bite(bool start) {
        if (start) { // Start bitting
            _biteTarget = null;
            Collider2D[] colliders = Physics2D.OverlapCircleAll((Vector2)transform.position, _meleeDistance, LayerMask.GetMask("Citizens"));
            for (int i = 0; i < colliders.Length; i++) {
                if (IsColliderInFrontOfUs(colliders[i])) {
                    _biteTarget = colliders[i].GetComponent<CitizenController>();
                    break;
                }
            }
            if (_biteTarget != null) { // Alright, we have a victim
                _isDraining = true; // Dracula is draining
                _runningDrainCoroutine = BloodDrainingCoroutine(); // Store draining coroutine's reference for later usage
                StartCoroutine(_runningDrainCoroutine); // Start draining coroutine
                _biteTarget.Bitten(true); // Notify Citizen that has been bitten
                _biteTarget.OnStateChange += BiteTarget_OnStateChange; // Subscribe to target's OnStateChange event
                _audioSource.PlayOneShot(_bitAudioClip); // Play bit audio clip
                _characterAnimator.SetInteger("Estado", (int)DraculaAnimationStates.Atacando);
                _characterAnimator.SetInteger("Accion", (int)DraculaAnimationActions.Mordiendo);
            } else { // We don't have a victim
                _characterAnimator.SetInteger("Estado", (int)DraculaAnimationStates.Atacando);
                _characterAnimator.SetInteger("Accion", (int)DraculaAnimationActions.Parado);
            }
        } else { // Stop bitting
            _characterAnimator.SetInteger("Estado", (state == CharacterState.Carrying) ? (int)DraculaAnimationStates.Cargando : (int)DraculaAnimationStates.Normal);
            _characterAnimator.SetInteger("Accion", (int)DraculaAnimationActions.Parado);

            if (_isDraining) {
                _isDraining = false; // Dracula is not draining
                StopCoroutine(_runningDrainCoroutine);  // Stop draining coroutine
                _biteTarget.OnStateChange -= BiteTarget_OnStateChange; // Unsubscribe from target's OnStateChange event
                _biteTarget.Bitten(false); // Notify Citizen that has been released
                _biteTarget = null;
            }
        }
    }

    public void PickUp() {
        if (_canCarry) {
            _carryingTarget = null;
            Collider2D[] colliders = Physics2D.OverlapCircleAll((Vector2)transform.position, _meleeDistance, LayerMask.GetMask("Corpses"));
            for (int i = 0; i < colliders.Length; i++) {
                if (IsColliderInFrontOfUs(colliders[i])) {
                    _carryingTarget = colliders[i].GetComponent<CitizenController>();
                    break;
                }
            }
            if (_carryingTarget != null) {
                state = CharacterState.Carrying;
                _previousCharacterState = CharacterState.Normal;
                _carryingTarget.PickedUp();
            }
        }
    }

    public void Drop() {
        if (state == CharacterState.Carrying && _carryingTarget != null) {
            bool hasHidingPlace = false;
            Collider2D[] colliders = Physics2D.OverlapCircleAll((Vector2)transform.position, _meleeDistance, LayerMask.GetMask("TransparentObjects", "UnwalkableObjects", "VisionBlockers"));
            for (int i = 0; i < colliders.Length; i++) {
                if (colliders[i].tag == "Escondite" && IsColliderInFrontOfUs(colliders[i])) {
                    hasHidingPlace = true;
                    break;
                }
            }
            if (hasHidingPlace) {
                _carryingTarget.gameObject.SetActive(false); // Disable the gameobject
            } else {
                _carryingTarget.Dropped(transform.position);
            }
            state = _previousCharacterState;
        }
    }

    public void Fly(bool start) {
        if (start && _currentBloodQuantity > _flyingMinimumBlood) {
            int modifiers = CountSpeedModifiers();
            DrainBlood(5 * modifiers);
            PruneSpeedModifiers();
            state = CharacterState.Flying;
            _runningFlyingCoroutine = FlyingCoroutine(); // Store draining coroutine's reference for later usage
            StartCoroutine(_runningFlyingCoroutine); // Start draining coroutine
            return;
        }
        if (!start && state == CharacterState.Flying) {
            StopCoroutine(_runningFlyingCoroutine);
            state = CharacterState.Normal;
        }
    }

    // Executed when bitted citizen's state changed
    private void BiteTarget_OnStateChange(CharacterState state) {
        if (state == CharacterState.Dead) { // The citizen has dead
            if (_isDraining)
                Bite(false); // Stop bitting
            if (OnKillCitizen != null)
                OnKillCitizen();
        }
    }

    private void IncrementBlood(float value) {
        _currentBloodQuantity += value;
        _currentBloodQuantity = Mathf.Clamp(_currentBloodQuantity, 0f, _maxBloodQuantity);
        if (OnBloodChange != null) {
            OnBloodChange(value, _currentBloodQuantity, _maxBloodQuantity);
        }
    }

    // Blood Draining Coroutine
    // It's executed while Dracula is draining a Citizen.
    IEnumerator BloodDrainingCoroutine() {
        while(true) {
            yield return new WaitForSeconds(1f); // Sleep Coroutine for a second
            if (_biteTarget) {
                int drainedBlood = _biteTarget.DrainBlood(Math.Min(_bloodPerSecond, (int)(_maxBloodQuantity - _currentBloodQuantity))); // Drain on BPS but not more than maximun blood quantity
                IncrementBlood(drainedBlood); // Increment current blood quantity with drained blood
                _audioSource.PlayOneShot(_drainAudioClip); // Play draining audo clip
            }
        }
    }

    IEnumerator FlyingCoroutine() {
        while (true) {
            DrainBlood(5 * Time.deltaTime);
            if (_currentBloodQuantity < _flyingMinimumBlood) {
                Fly(false);
            }
            yield return null;
        }
    }

    public void Kill() {
        state = CharacterState.Dead;
        MessageManager.ShowMessage("Has muerto");
        Invoke("SendKillEvent", 4.0f);
    }

    public void SendKillEvent() {
        if (OnDraculaDead != null)
            OnDraculaDead();
    }

    public void Blind(float power) {
        if (state != CharacterState.Blinded) {
            SpeedModifier modifier = new SpeedModifier(-power);
            AddSpeedModifier(SpeedModifierTypes.HolyCross, modifier);
            state = CharacterState.Blinded;
        }
    }

    public void UnBlind() {
        if (state == CharacterState.Blinded) {
            RemoveSpeedModifier(SpeedModifierTypes.HolyCross);
            state = _previousCharacterState;
        }
    }

    public void DrainBlood(float damage) {
        IncrementBlood(-damage);
        if (_currentBloodQuantity == 0f)
            Kill();
    }

    public override CharacterState state {
        set {
            SpeedModifier modifier;
            base.state = value;
            switch (value) {
                case CharacterState.Normal:
                    _characterAnimator.SetInteger("Estado", (int)DraculaAnimationStates.Normal);
                    break;
                case CharacterState.Blinded:
                    if (_previousCharacterState == CharacterState.Carrying) {
                        _carryingTarget.Dropped(transform.position);
                        _previousCharacterState = CharacterState.Normal;
                    }
                    _characterAnimator.SetInteger("Estado", (int)DraculaAnimationStates.Cegado);
                    break;
                case CharacterState.Carrying:
                    _characterAnimator.SetInteger("Estado", (int)DraculaAnimationStates.Cargando);
                    if (_carryingDebuffPower > 0) {
                        modifier = new SpeedModifier(-_carryingDebuffPower);
                        AddSpeedModifier(SpeedModifierTypes.Carrying, modifier);
                    }
                    break;
                case CharacterState.Flying:
                    _characterAnimator.SetInteger("Estado", (int)DraculaAnimationStates.Volando);
                    modifier = new SpeedModifier(speedMultiplicator);
                    AddSpeedModifier(SpeedModifierTypes.Flying, modifier);
                    gameObject.layer = 14;

                    if (_previousCharacterState == CharacterState.Carrying) {
                        _carryingTarget.Dropped(transform.position);
                        _previousCharacterState = CharacterState.Normal;
                    }
                    break;
                case CharacterState.Dead:
                    _characterAnimator.SetInteger("Accion", (int)DraculaAnimationActions.Muerto);
                    break;
            }
        }
    }
}
