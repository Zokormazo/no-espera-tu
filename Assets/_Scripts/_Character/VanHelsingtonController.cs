﻿using UnityEngine;

public class VanHelsingtonController : MyCharacterController {

    [Header("Attack")]
    [Tooltip("Melee considered distance")]
    [SerializeField]
    private float _attackDistance = 0.4f;

    // Called every frame that cop has a potential shoot.
    public override void Fire(DraculaController dracula) {
        if (dracula.alive && Vector2.Distance(transform.position, dracula.transform.position) < _attackDistance) {
            _characterAnimator.SetTrigger("Fire");
            dracula.Kill();
        }
    }

    public float attackDistance {
        get { return _attackDistance; }
    }
}
