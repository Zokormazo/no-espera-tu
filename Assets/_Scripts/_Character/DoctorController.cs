﻿using UnityEngine;

public enum DoctorAnimationActions {
    Parado = 0,
    Andando = 1,
}

public class DoctorController : MyCharacterController {
    [Header("Attack")]
    [Tooltip("Drained blood per second")]
    [SerializeField]
    private float _attackDPS;
    [Tooltip("Melee considered distance")]
    [SerializeField]
    private float _attackDistance = 0.4f;

    // Called every frame that cop has a potential shoot.
    public override void Fire(DraculaController dracula) {
        if (dracula.alive && Vector2.Distance(transform.position, dracula.transform.position) < _attackDistance) {
            _characterAnimator.SetTrigger("Fire");
            dracula.DrainBlood(_attackDPS*Time.deltaTime);
        }
    }

    public float attackDistance {
        get { return _attackDistance; }
    }
}
