﻿/* 
 * CitizenController.cs
 * Citizen's main controller class.
 */
using System.Collections.Generic;
using UnityEngine;

public enum ShoutSeen {
    Dracula,
    Corpse
}

public enum CitizenAnimationActions {
    Parado = 0,
    Andando = 1,
    Huyendo = 2,
    Atrapado = 3,
    Muerto = 4,
    RezandoRapido = 5
}

[RequireComponent(typeof(AudioSource))]
public class CitizenController : MyCharacterController {

    // Variables exported to Unity Editor's Inspector
    [Header("Blood")]
    [Tooltip("Initial & Maximun Blood Quantity")]
    [SerializeField]
    private int _initialBloodQuantity; // Specifies initial & maximun blood quantiy
    [Tooltip("Blood Restoration Rate (BPS)")]
    private int _bloodPerSecond;

    [Header("Shout")]
    [Tooltip("Range of the shout")]
    [SerializeField]
    private float _shoutRange;
    [Tooltip("Enable/Disable shout system")]
    [SerializeField]
    private bool _shoutEnabled;

    [Header("Sound Effects")]
    [Tooltip("Shout Dracula! sound effect")]
    [SerializeField]
    private AudioClip _shoutDraculaSFX;
    [Tooltip("Shout Corpse! sound effect")]
    [SerializeField]
    private AudioClip _shoutCorpseSFX;

    [Header("UI")]
    [Tooltip("Character's photo for UI")]
    [SerializeField]
    private Texture _photo;

    // Components
    private AudioSource _citizenAudioSource;

    // Game Behaviour variables
    private int _currentBloodQuantity; // Current blood quantity
    private bool _canShout;

    // Show stats related
    public delegate void ShowCitizenStats(CitizenController citizen);
    public static event ShowCitizenStats OnShowStats;
    public delegate void UpdateHealthBar();
    public event UpdateHealthBar OnUpdateHealthBar;
    private bool _isSelected;

    private HashSet<int> seenCorpses;

    /*
     * Methods called by Unity
     */

    // Use this for initialization
    private new void Start() {
        base.Start(); // Exec parent's Start
        seenCorpses = new HashSet<int>();
        _currentBloodQuantity = _initialBloodQuantity; // Initialize current blood quantity
        _citizenAudioSource = GetComponent<AudioSource>(); // Get citizen's Audiosource for later usage
        gameObject.layer = 12;
    }

    private void OnMouseEnter() {
        if (OnShowStats != null)
            OnShowStats(this);
    }

    private void OnDestroy() {
        OnShowStats = null;
        OnUpdateHealthBar = null;
    }

    /*
     * Public action methods
     */

    // Shout that we've seen Dracula/Corpse
    public void Shout(ShoutSeen seenElement, Vector2 point, int instanceID = 0) {
        if (_canShout && _shoutEnabled) { // We can actually shout
            Collider2D[] listenerColliders = Physics2D.OverlapCircleAll(transform.position, _shoutRange, LayerMask.GetMask("ShoutListeners")); // Get the listener's collider inside shouting range
            switch (seenElement) {
                case ShoutSeen.Dracula:
                    _citizenAudioSource.PlayOneShot(_shoutDraculaSFX); // Play audio clip
                    foreach (Collider2D listener in listenerColliders) // Iterate over listener's in range
                        listener.GetComponent<CopCPUControl>().OnAlertDracula(point); // Alert listener that we've seen dracula
                    break;
                case ShoutSeen.Corpse:
                    if (!seenCorpses.Contains(instanceID)) {
                        _citizenAudioSource.PlayOneShot(_shoutCorpseSFX); // Play audio clip
                        seenCorpses.Add(instanceID);
                    }
                    foreach (Collider2D listener in listenerColliders) // Iterate over listener's in range
                        listener.GetComponent<CopCPUControl>().OnAlertCorpse(point, instanceID); // Alert listener that we've seen a corpse
                    break;
            }
        }
    }

    // Got Bitten-Released by Dracula.
    public void Bitten(bool start) {
        if (alive) {
            if (start) {
                state = CharacterState.Stunned;
            }
            else if (_currentBloodQuantity >= _initialBloodQuantity / 2) {
                state = CharacterState.Escaping;
            }
        }
    }

    // Drain specified blood quantity
    // Returns the real quantity that has been drained. Kill citizen if goes out of blood.
    public int DrainBlood(int quantity) {
        if (OnUpdateHealthBar != null)
            OnUpdateHealthBar();
        if (_currentBloodQuantity > quantity) { // Citizen's is drained but not dead yet.
            _currentBloodQuantity -= quantity; // Drain live
            return quantity; // Return full draining quantity as drained quantity
        } else { // Citizen out of blood. Dead.
            int result = _currentBloodQuantity; // Store current blood quantity to return as drained quantity
            _currentBloodQuantity = 0; // Set blood quantity to 0
            if (OnUpdateHealthBar != null)
                OnUpdateHealthBar();
            state = CharacterState.Dead; // Change character's state
            return result; // Return just the actually drained quantity, lower than intented draining quantity
        }
    }

    // Got picked up by dracula
    public void PickedUp() {
        Show(false); // Hide
    }

    // Got Dropped by dracula on the given position
    public void Dropped(Vector3 position) {
        transform.position = position; // Update position to the given one
        Show(true); // Show the corpse
    }

    // Hide / show the citizen
    public void Show(bool value) {
        _characterSpriteRenderer.enabled = value; // Enable/Disable the Sprite Renderer
        if (alive) {
            _characterBoxCollider2D.enabled = value; // Enable/Disable the Box Collider
            if (!value) // Return to normal state when hidden and alive
                state = CharacterState.Normal;
        }
    }

    /*
     * Public properties
     */

    // Get/Set character's state
    public override CharacterState state {
        set
        {
            base.state = value;
            switch (value) {
                case CharacterState.Normal:
                    _canShout = true;
                    _characterAnimator.SetInteger("Accion", (int)CitizenAnimationActions.Parado);
                    break;
                case CharacterState.Immobilized:
                    _canShout = true;
                    break;
                case CharacterState.Escaping:
                    CitizenCPUControl control = GetComponent<CitizenCPUControl>();
                    control.Escape();
                    break;
                case CharacterState.Stunned:
                    _characterAnimator.SetInteger("Accion", (int)CitizenAnimationActions.Atrapado);
                    if (OnShowStats != null)
                        OnShowStats(this);
                    _canShout = false;
                    break;
                case CharacterState.Dead:
                    _characterAnimator.SetInteger("Accion", (int)CitizenAnimationActions.Muerto);
                    gameObject.layer = 13;
                    _canShout = false;
                    break;
            }
        }
    }

    public bool injured {
        get { return _initialBloodQuantity > _currentBloodQuantity;  }
    }

    public string citizenName {
        get { return gameObject.name; }
    }

    public Texture citizenPhoto {
        get { return _photo; }
    }

    public int currentBlood {
        get { return _currentBloodQuantity; }
    }

    public int maxBlood {
        get { return _initialBloodQuantity; }
    }

    public bool selected {
        get { return _isSelected; }
        set { _isSelected = value; }
    }
}
