﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaDePinchos : MonoBehaviour
{
    public Animator _animator;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            _animator.SetInteger("Accion", 1);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            _animator.SetInteger("Accion", 2);
        }
    }
}
