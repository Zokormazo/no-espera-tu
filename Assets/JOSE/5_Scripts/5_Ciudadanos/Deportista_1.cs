﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deportista_1 : MonoBehaviour
{
    private SpriteRenderer _axis;
    public Animator _animator;
    private bool _muerto = false;


    // Use this for initialization
    void Start()
    {
        _axis = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        int _accion = _animator.GetInteger("Accion");
        if (_accion == 4)
        {
            _muerto = true;
        }
        if (_muerto == false)
        {
            if (Input.GetKey(KeyCode.E))
            {
                _animator.SetInteger("Accion", 2);
            }
            if (Input.GetKeyUp(KeyCode.E))
            {
                _animator.SetInteger("Accion", 0);
            }
            if (Input.GetKey(KeyCode.D))
            {
                _axis.flipX = true;
                float _andar = Time.deltaTime * 3;
                transform.Translate(_andar, 0, 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Accion", 2);
                }
            }
            if (Input.GetKey(KeyCode.A))
            {
                _axis.flipX = false;
                float _andar = Time.deltaTime * 3;
                transform.Translate(-_andar, 0, 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Accion", 2);
                }
            }
            if (Input.GetKey(KeyCode.W))
            {
                float _andar = Time.deltaTime * 3;
                transform.Translate(0, _andar, 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Accion", 2);
                }
            }
            if (Input.GetKey(KeyCode.S))
            {
                float _andar = Time.deltaTime * 3;
                transform.Translate(0, -_andar, 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Accion", 2);
                }
            }
        }


        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
        {
            _animator.SetInteger("Accion", 0);
        }
    }
}
