﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heladera : MonoBehaviour
{
    private SpriteRenderer _axis;
    private Animator _animator;
    private bool Muerto = false;

    // Use this for initialization
    void Start ()
    {
        _axis = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        int _accion = _animator.GetInteger("Accion");
        if (_accion == 2)
        {
            Muerto = true;
        }
        if (Input.GetKey(KeyCode.E))
        {
            _animator.SetInteger("Estado", 2);
            //_animator.SetInteger("Accion", 1);
        }
        if (Muerto == false)
        {
            if (Input.GetKey(KeyCode.D))
            {
                _axis.flipX = true;
                float _andar = Time.deltaTime * 3;
                transform.Translate(_andar, 0, 0);
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Estado", 2);
                    _animator.SetInteger("Accion", 1);
                }
                // Para leer el estado y la accion que tiene guardados el Animator Controller
                //int estado = _animator.GetInteger("DraculaEstado");
                //int accion = _animator.GetInteger("DraculaAcciones");
                //-------------------------------------------------------------------------------------------------------
                // Para escribir el estado y la accion en las variables del Animator Controller
                //_animator.SetInteger("DraculaEstado", 1);
                //_animator.SetInteger("DraculaAcciones", 2);
            }
            if (Input.GetKey(KeyCode.A))
            {
                _axis.flipX = false;
                float _andar = Time.deltaTime * 3;
                transform.Translate(-_andar, 0, 0);
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Estado", 2);
                    _animator.SetInteger("Accion", 1);
                }
            }
            if (Input.GetKey(KeyCode.W))
            {
                float _andar = Time.deltaTime * 3;
                transform.Translate(0, _andar, 0);
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Estado", 2);
                    _animator.SetInteger("Accion", 1);
                }
            }
            if (Input.GetKey(KeyCode.S))
            {
                float _andar = Time.deltaTime * 3;
                transform.Translate(0, -_andar, 0);
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Estado", 2);
                    _animator.SetInteger("Accion", 1);
                }
            }
            // Para leer el estado y la accion que tiene guardados el Animator Controller
            /*int estado = _animator.GetInteger("DraculaEstado");
            int accion = _animator.GetInteger("DraculaAcciones");
            if (estado == 0 && accion == 1)
            {
                _animator.SetInteger("DraculaAcciones", 0);
            }
            */
            if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.E))
            {
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 0);
            }
        }


    }
}

