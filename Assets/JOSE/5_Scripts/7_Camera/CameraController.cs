﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    private Transform _position;
    private Camera _size;
    private UserControl _userControl;
    private float _time;
    private float _timeTotal = 9;
    public float _DampTime = 0.3f;
    private float _ZoomSpeed;
    private Vector3 _MoveSpeed;

    private void Awake()
    {
        _position = GetComponent<Transform>();
        _size = GetComponent<Camera>();
        _userControl = FindObjectOfType<UserControl>();
    }


    // Use this for initialization
    void Start()
    {
        _userControl.enabled = false;
        _position.position = new Vector3(-7.87f, 2.59f, -1.41f);
        _size.orthographicSize = 0.49f;
    }
	// Update is called once per frame
	void Update ()
    {
        _time += Time.deltaTime;
        if(_time >= _timeTotal)
        {
            _position.position = Vector3.SmoothDamp(_position.position, new Vector3(0f, 0f, -10f), ref _MoveSpeed, _DampTime);
            _size.orthographicSize = Mathf.SmoothDamp(_size.orthographicSize, 5f, ref _ZoomSpeed, _DampTime);
            _size.orthographicSize = Mathf.Clamp(_size.orthographicSize, 0.49f, 5f);
            _userControl.enabled = true;

            if (_time >= 14f)
                this.enabled = false;
        }
    }
}
