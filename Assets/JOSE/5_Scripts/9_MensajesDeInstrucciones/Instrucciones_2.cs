﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instrucciones_2 : MonoBehaviour
{
    private Animator _animator;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Dracula")
        {
            _animator.SetInteger("Accion", 1);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Dracula")
        {
            _animator.SetInteger("Accion", 0);
            gameObject.SetActive(false);
        }
    }
}