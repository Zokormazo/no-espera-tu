﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Ataud : MonoBehaviour
{

    private Animator _animator;
    private MurcielagoTranscision _transferencia;
    //private TranscisionAnimator;
    private float _tiempo = 0.0f;
    private float _timeTotal = 4.3f;
    public bool vuelta_3 = false;
    //public GUIText _tiempoTex;

    public bool AbrirseAlPrincipio = true;

    // Use this for initialization
    void Start ()
    {
        _animator = GetComponent<Animator>();
        _transferencia = FindObjectOfType<MurcielagoTranscision>();
    }

    // Update is called once per frame
    void Update ()
    {
        if (AbrirseAlPrincipio) {                              //>>>>>>>>>>>>>>>>>>>>>>>BLOQUE DE IDA>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                                               //Inicio la accion automaticamente preguntando por la variable del animator que por descontado esta en "0" 
            if (_animator.GetInteger("Accion") == 0) {
                //Lanzamos la primera animacion que desatara el resto de acciones atomatizadas en este gameobject del "Ataud"
                _animator.SetInteger("Accion", 1);
            }
            //Ahora esperamos un tiempo de "4.3" segundos para prosegir conel envio de la variable al script del gameobject "MurcielagoTranscision"
            if (vuelta_3 == false) {
                _tiempo += Time.deltaTime;
                if (_tiempo >= _timeTotal)// && _animator.GetInteger("Accion") == 1
                {
                    //Pasado el tiempo no solo le enviamos la variable antes activamos el script del game object "MurcielagoTranscision"
                    _animator.SetInteger("Accion", 2);
                    _transferencia.enabled = true;       //Activo el script
                    _transferencia.ida_1 = true;         //Lanzo el valor al script
                                                         //Y finalizo desactivando este script que ya no necesito que se ejecute
                    enabled = false;
                }
            }
        }
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<BLOQUE DE VUELTA<<<<<<<<<<<<<<<<<<<<<<

        //Recivimos la variable del script del gameobject "MurcielagoTranscision"
        if (vuelta_3 == true)
        {
            vuelta_3 = false;
            _animator.enabled = true;
            _animator.SetInteger("Accion", 3);
            _animator.SetBool("DraculaIsTrue", true);
            enabled = false;
        }
	}
}
