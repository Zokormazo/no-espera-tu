﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaudPuerto : MonoBehaviour {
    private Animator _animator;
    public bool vuelta_3 = false;
    //public GUIText _tiempoTex;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<BLOQUE DE VUELTA<<<<<<<<<<<<<<<<<<<<<<

        //Recivimos la variable del script del gameobject "MurcielagoTranscision
        if (vuelta_3 == true)
        {
            vuelta_3 = false;
            _animator.enabled = true;
            _animator.SetInteger("Accion", 3);
            _animator.SetBool("DraculaIsTrue", true);
            enabled = false;
        }
    }
}
