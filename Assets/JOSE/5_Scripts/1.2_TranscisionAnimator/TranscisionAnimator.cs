﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranscisionAnimator : MonoBehaviour {

    private Stop _dracula;
    private MurcielagoTranscision _transferencia;
    private Animator _animator;
    public bool ida_2 = false;
    public bool vuelta_1 = false;
    private float tiempo = 0.0f;
    private float timeTotal;

    private void Awake()
    {
        
    }

    // Use this for initialization
    void Start ()
    {
        _dracula = FindObjectOfType<Stop>();
        _transferencia = FindObjectOfType<MurcielagoTranscision>();
        _animator = GetComponent<Animator>();
        _animator.SetBool("DT", false);
    }
	
	// Update is called once per frame
	void Update ()
    {
                                            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BLOQUE DE IDA>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		if(ida_2 == true)
        {
            _animator.SetBool("DT", true);
            timeTotal = 3.55f;
            tiempo += Time.deltaTime;
            if (tiempo >= timeTotal)
            {
                tiempo = 0;
                ida_2 = false;
                _dracula.enabled = true;
                _dracula.ida_3 = true;
                enabled = false;
            }
        }

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<BLOQUE DE VUELTA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        if(vuelta_1 == true)
        {
            _animator.enabled = true;
            timeTotal = 4.01f;
            _animator.SetBool("DT", false);
            tiempo += Time.deltaTime;
            if (tiempo >= timeTotal)
            {
                vuelta_1 = false;
                tiempo = 0;
                _transferencia.enabled = true;
                _transferencia.vuelta_2 = true;
                enabled = false;
            }
        }
	}
}
