﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparador : MonoBehaviour {

    private Animator _animator;
    private TranscisionAnimator _transformacion;
    public bool _entrarAlAtaud = false; //Variable Que puedes usar Julen para mandar la orden de que entre en el ataud y mas condiciones claro.

	// Use this for initialization
	void Awake () {
        _animator = GetComponent<Animator>();
        _transformacion = FindObjectOfType<TranscisionAnimator>();
    }
	
	// Update is called once per frame
	void Start () {
        _animator.SetBool("Aparece", true);
    }
    void Update()
    {
        //<<<<<<<<<<<<<<<<<<<<<<<<<BLOQUE DE SALIDA, INCIAMOS EL REGRESO DE DRACULA AL ATAUD<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //AQUI SE ORGANIZA LA VUELTA.
        if (_entrarAlAtaud == true) // && "SiTieneLaSangreSuficiente && MatoLaSuficienteGente && ETC"")//Como apoyo 
        {                                             //                                    
            _transformacion.enabled = true;           //     Activo el archivo "TranscisionAnimator" para prepararlo y 
            _transformacion.vuelta_1 = true;          //     aqui le mandamos el valor boleano al script "TranscisionAnimator".
            _entrarAlAtaud = false;
            StartCoroutine(Wait(1f));                 //     Llamamos a una corrutina que nos hara esperar un tiempo en segundos antes de lanzar las acciones correspondientes.
        }                                             //
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Fin del bloque para las transcisiones de vuelta.<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    }
    IEnumerator Wait(float time)              //     LANZAMOS LA CORRUTINA CON LA CONDICION DE ESPERA
    {
        yield return new WaitForSeconds(time);//     
        _animator.SetBool("Aparece", false);  //     Aqui nos aseguramos de cambiar el valor boleano que quedo anteriormente en la variable, sino hace y desace...
        _animator.SetBool("Entra", true);     //     Le damos la orden al animator para dar paso al sprite transparente.
        enabled = false;                      //     Desactivo el script para detener su ejecucion.
    }
}
