﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Stop : MonoBehaviour
{

    private Animator _animator;
    private UserControl _userControl;
    public bool ida_3 = false;
    private float tiempo = 0.0f;
    private float timeTotal = 1.5f;
    private int Control = 0;
    private bool _enabledUserControl;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();
        _userControl = GetComponent<UserControl>();
        _userControl.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BLOQUE FINAL DE LA TRANSFERENCIA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        if (ida_3 == true && Control == 0)            //Recivo la condicion del script "TranscisionAnimator" y entramos en el "if"
        {
            Control = 1;                              //Preparo la variable con el valor que nos interesa que entre en la siguiente condicion.
            _animator.SetBool("Aparece", true);       //Y hacemos aparecer al protagonista.
        }
        if(Control == 1)                              //Preguntamos por esa variable que cuando tenga el valor que se pide entre.
        {
            tiempo += Time.deltaTime;                 //Pongo en marcha un contador de tiempo para controlar que la condicion se active un segundo despues.
            if (tiempo >= timeTotal && !_enabledUserControl)                  //Llegado al tiempo, entramos.
            {
                _userControl.enabled = true;          //Y permitimos la avilitacion del archivo o script "UserControler".
                _enabledUserControl = true;
            }
        }
        //<<<<<<<<<<<<<<<<<<<<<<<<<BLOQUE DE SALIDA Y VUELTA HACIA ATRAS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //AQUI SE ORGANIZA LA VUELTA.
        /*if (Input.GetKeyDown(KeyCode.T) && Control == 1)//COMENTAR ESTO ES SOLO PARA MIS PRUEBAS La condicion cambia dependiendo de como se programe aparte.
        {                                             //                                 
            Control = 2;                              //     A esta variable le cambiamos el valor para asegurarnos de que ya no vuelve a entrar aqui
            ida_3 = false;                            //     Con esto evito que entre en el "if" de arriba y ahora si que entre en el tercero o el siguiente.
            _transformacion.enabled = true;           //     Activo el archivo "TranscisionAnimator" para prepararlo y 
            _transformacion.vuelta_1 = true;          //     aqui le mandamos el valor boleano al script "TranscisionAnimator".
        }                                             //
        if (ida_3 == false && Control == 2)           //     Comprobamos si cumple las dos condiciones para entrar.
        {                                             //
            StartCoroutine(Wait(1f));                 //     Llamamos a una corrutina que nos hara esperar un tiempo en segundos antes de lanzar las acciones correspondientes.
        }//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Fin del bloque para las transcisiones de vuelta.<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        */
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        //--------------------------------------------------------------------BLOQUE DE MOVIMIENTOS---------------------------------------------------------------------------------------
    }

    IEnumerator Wait(float time)              //     lANZAMOS LA CORRUTINA CON LA CONDICION DE ESPERA AL SCRIPT "TRANSCISIONANIMATOR"
    {
        yield return new WaitForSeconds(time);//     
        _animator.SetBool("Entra", true);     //     Le damos la orden al animator para dar paso al sprite transparente.
        _animator.SetBool("Aparece", false);  //     Aqui nos aseguramos de cambiar el valor boleano que quedo anteriormente en la variable, sino hace y desace...
        enabled = false;                      //     Desactivo el script para deter su ejecucion.
    }
}