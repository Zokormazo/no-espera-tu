﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class DraculaSeMueve : MonoBehaviour
{

    private TranscisionAnimator _transformacion;
    private SpriteRenderer _axis;
    private Animator _animator;
    private bool Muerto = false;
    public bool ida_3 = false;
    private int Control = 0;

    void Awake()
    {
        enabled = false;
    }

    // Use this for initialization
    void Start () {
        _transformacion = FindObjectOfType<TranscisionAnimator>();
        _axis = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BLOQUE FINAL DE LA TRANSFERENCIA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        if (ida_3 == true && Control == 0)            //Recivo la condicion del script "TranscisionAnimator" y entramos en el "if"
        {
            Control = 1;
            _animator.SetBool("Aparece", true);       //Y hacemos aparecer al protagonista
            //_onOf_3 = false;                        //Si le doy este valor mejor solo cuando pulse la "T" me aseguro de no entrar en el tercer "if"
        }

        //<<<<<<<<<<<<<<<<<<<<<<<<<BLOQUE DE SALIDA Y VUELTA HACIA ATRAS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                                                        //AQUI SE ORGANIZA LA VUELTA.
        if (Input.GetKeyDown(KeyCode.T) && Control == 1)//COMENTAR ESTO ES SOLO PARA MIS PRUEBAS La condicion cambia dependiendo de como se programe aparte.
        {                                             //                                 
            Control = 2;                              //     A esta variable le cambiamos el valor para asegurarnos de que ya no vuelve a entrar aqui
            ida_3 = false;                            //     Con esto evito que entre en el "if" de arriba y ahora si que entre en el tercero o el siguiente.
            _transformacion.enabled = true;           //     Activo el archivo "TranscisionAnimator" para prepararlo y 
            _transformacion.vuelta_1 = true;          //     aqui le mandamos el valor boleano al script "TranscisionAnimator".
        }                                             //
        if (ida_3 == false && Control == 2)           //     Comprobamos si cumple las dos condiciones para entrar.
        {                                             //
            StartCoroutine(Wait(1f));                 //     Llamamos a una corrutina que nos hara esperar un tiempo en segundos antes de lanzar las acciones correspondientes.
        }//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Fin del bloque para las transcisiones de vuelta.<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        //--------------------------------------------------------------------BLOQUE DE MOVIMIENTOS---------------------------------------------------------------------------------------
    
        int _accion = _animator.GetInteger("Accion");
        if (_accion == 2)
        {
            Muerto = true;
        }
        if (Input.GetKey(KeyCode.E))
        {
            _animator.SetInteger("Estado", 2);
            _animator.SetInteger("Accion", 1);
        }
        if (Muerto == false)
        {
            if (Input.GetKey(KeyCode.D))
            {
                _axis.flipX = false;
                float _andar = Time.deltaTime * 3;
                transform.Translate(_andar, 0, 0);
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Estado", 2);
                    _animator.SetInteger("Accion", 1);
                }
                // Para leer el estado y la accion que tiene guardados el Animator Controller
                //int estado = _animator.GetInteger("DraculaEstado");
                //int accion = _animator.GetInteger("DraculaAcciones");
                //-------------------------------------------------------------------------------------------------------
                // Para escribir el estado y la accion en las variables del Animator Controller
                //_animator.SetInteger("DraculaEstado", 1);
                //_animator.SetInteger("DraculaAcciones", 2);
            }
            if (Input.GetKey(KeyCode.A))
            {
                _axis.flipX = true;
                float _andar = Time.deltaTime * 3;
                transform.Translate(-_andar, 0, 0);
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Estado", 2);
                    _animator.SetInteger("Accion", 1);
                }
            }
            if (Input.GetKey(KeyCode.W))
            {
                float _andar = Time.deltaTime * 3;
                transform.Translate(0, _andar, 0);
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Estado", 2);
                    _animator.SetInteger("Accion", 1);
                }
            }
            if (Input.GetKey(KeyCode.S))
            {
                float _andar = Time.deltaTime * 3;
                transform.Translate(0, -_andar, 0);
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 1);
                if (Input.GetKey(KeyCode.E))
                {
                    _animator.SetInteger("Estado", 2);
                    _animator.SetInteger("Accion", 1);
                }
            }
            // Para leer el estado y la accion que tiene guardados el Animator Controller
            /*int estado = _animator.GetInteger("DraculaEstado");
            int accion = _animator.GetInteger("DraculaAcciones");
            if (estado == 0 && accion == 1)
            {
                _animator.SetInteger("DraculaAcciones", 0);
            }
            */
            if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.E))
            {
                _animator.SetInteger("Estado", 0);
                _animator.SetInteger("Accion", 0);
            }
        }
        
        
    }
    IEnumerator Wait(float time)              //     lANZAMOS LA CORRUTINA CON LA CONDICION DE ESPERA
    {
        yield return new WaitForSeconds(time);//     
        _animator.SetBool("Entra", true);     //     Le damos la orden al animator para dar paso al sprite transparente.
        _animator.SetBool("Aparece", false);  //     Aqui nos aseguramos de cambiar el valor boleano que quedo anteriormente en la variable, sino hace y desace...
        enabled = false;                      //     Desactivo el script para deter su ejecucion.
    }
}
