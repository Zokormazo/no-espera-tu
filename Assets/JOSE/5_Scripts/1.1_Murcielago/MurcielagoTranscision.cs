﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurcielagoTranscision : MonoBehaviour
{
    private TranscisionAnimator _transformacion;
    private Ataud _principio;
    private Animator _animator;
    public bool ida_1 = false;
    public bool vuelta_2 = false;

    void Start()
    {
        _transformacion = FindObjectOfType<TranscisionAnimator>();
        _principio = FindObjectOfType<Ataud>();
        _animator = GetComponent<Animator>();
        _animator.SetBool("OnOf", false);
    }

    // Update is called once per frame
    void Update()
    {
                                                       //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>bLOQUE DE IDA>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        if (ida_1 == true)
        {
            _animator.SetBool("OnOf", true);
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(0.45f, transform.localPosition.y, transform.localPosition.z), Time.deltaTime / 2f);
            if (transform.localPosition.x >= 0.45f)
            {
                ida_1 = false;
                _transformacion.enabled = true;
                _transformacion.ida_2 = true;
                _animator.SetBool("OnOf", false);
                enabled = false;
            }
        }

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<BLOQUE DE VUELTA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        if (vuelta_2 == true)
        {
            _animator.enabled = true;
            _animator.SetBool("OnOf", true);
            transform.localPosition = Vector2.MoveTowards(transform.localPosition, new Vector3(0.035f, transform.localPosition.y, transform.localPosition.z), Time.deltaTime / 2f);
            if (transform.localPosition.x <= 0.035f)
            {
                vuelta_2 = false;
                _principio.enabled = true;
                _principio.vuelta_3 = true;
                StartCoroutine(Wait(1.5f));
                enabled = false;
            }
        }
        
    }

    IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
        vuelta_2 = false;
        _animator.SetBool("OnOf", false);
    }
}
