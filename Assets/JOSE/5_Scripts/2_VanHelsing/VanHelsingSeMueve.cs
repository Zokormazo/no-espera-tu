﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VanHelsingSeMueve : MonoBehaviour
{
    private SpriteRenderer _axis;
    public Animator _animator;


    // Use this for initialization
    void Start()
    {
        _axis = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Con la primera pregunta da fallo a la hora de mantener la accion de atacando cuando se deja de andar, y
        //si funciona si se vuelve a presionar pero la logica es que si se sigue apretando siga en el 
        //estado que le corresponde.
        //if (Input.GetKeyDown(KeyCode.E))
        if (Input.GetKey(KeyCode.E))
        {
            _animator.SetInteger("Accion", 2);
        }
        //Preguntamos por si dejamos de pulsar la "E" para volver al estado de parado, en este caso.
        if (Input.GetKeyUp(KeyCode.E))
        {
            _animator.SetInteger("Accion", 0);
        }
        //Los siguientes "if" preguntan por la direccion que tomara el personaje y cambiara al estado de la 
        //animacion que corresponda, asi como las direcciones de las "flipX y flipY", pero anidan otra pregunta
        //que hace que cambie a otra accion animada, en este caso claro, atacar.
        if (Input.GetKey(KeyCode.D))
        {
            _axis.flipX = true;
            float _andar = Time.deltaTime * 3;
            transform.Translate(_andar, 0, 0);
            _animator.SetInteger("Accion", 1);
            //Con esto, preguntamos si pulsamos "E" y cambiamos la accion de la animacion por "Atacando"
            if (Input.GetKey(KeyCode.E))
            {
                _animator.SetInteger("Accion", 2);
            }
        }
        if (Input.GetKey(KeyCode.A))
        {
            _axis.flipX = false;
            float _andar = Time.deltaTime * 3;
            transform.Translate(-_andar, 0, 0);
            _animator.SetInteger("Accion", 1);
            if (Input.GetKey(KeyCode.E))
            {
                _animator.SetInteger("Accion", 2);
            }
        }
        if (Input.GetKey(KeyCode.W))
        {
            float _andar = Time.deltaTime * 3;
            transform.Translate(0, _andar, 0);
            _animator.SetInteger("Accion", 1);
            if (Input.GetKey(KeyCode.E))
            {
                _animator.SetInteger("Accion", 2);
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            float _andar = Time.deltaTime * 3;
            transform.Translate(0, -_andar, 0);
            _animator.SetInteger("Accion", 1);
            if (Input.GetKey(KeyCode.E))
            {
                _animator.SetInteger("Accion", 2);
            }
        }
        //Con esta pregunta consegimos saber si dejamos de pulsar cualquiera de las teclas con las que movemos al personaje y devolver al personaje al estado de animacion "parado"
        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
        {
            _animator.SetInteger("Accion", 0);
        }
    }
}
