﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Murcielag_1 : MonoBehaviour
{
    private SpriteRenderer _axis;
    private int _paso = 0;

    // Use this for initialization
    void Start()
    {
        _axis = GetComponent<SpriteRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        if (_paso == 0)
        {
            _axis.flipX = true;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(1f, transform.localPosition.y, transform.localPosition.z), Time.deltaTime / 2f);
            if (transform.localPosition.x >= 1f)
            {
                _paso = 1;
            }
        }
        if (_paso == 1)
        {
            _axis.flipX = false;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(-5f, transform.localPosition.y, transform.localPosition.z), Time.deltaTime / 2f);
            if (transform.localPosition.x <= -5f)
            {
                _paso = 0;
            }
        }
    }
}
